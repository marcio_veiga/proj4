﻿using Foundation;
using SparkleIT.IPVC.YourAccess.Mobile.iOS.Helpers;
using System;
using System.Collections.Specialized;
using UIKit;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace SparkleIT.IPVC.YourAccess.Mobile.iOS
{
    public partial class Home : UIViewController
    {
        public Home(IntPtr handle) : base(handle) { }
        
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            btRefresh.Clicked += HandleRefresh;
            btLogout.Clicked += HandleLogout;

            getMessages();

            try
            {
                // Get Shared User Defaults
                var plist = NSUserDefaults.StandardUserDefaults;
                var user_id = plist.StringForKey("User_ID");
                var username = plist.StringForKey("Username");

                lbUsername.Text += username;

                try
                {
                    // Get QRCode
                    var response = Http.Post("http://192.168.43.59:64001/auth/qrcode", new NameValueCollection() {
                        { "id", user_id },
                        { "username", username }
                    });

                    string result = System.Text.Encoding.UTF8.GetString(response);
                    result = result.Replace("\"", string.Empty);

                    // Using QRCode
                    var barcodeWriter = new ZXing.Mobile.BarcodeWriter
                    {
                        Format = ZXing.BarcodeFormat.QR_CODE,
                        Options = new ZXing.Common.EncodingOptions
                        {
                            Width = 200,
                            Height = 200
                        }
                    };

                    if (result.Equals("false"))
                    {
                        UIAlertView alert = new UIAlertView()
                        {
                            Title = "Error",
                            Message = "No QRCode provided for current user."
                        };
                        alert.AddButton("OK");
                        alert.Show();

                        Logout();
                    }
                    else
                    {
                        // Generate QRCode
                        var barcode = barcodeWriter.Write(result);
                        // Show QrCode
                        ivQrcode.Image = barcode;
                    }
                }
                catch (Exception) // Error getting QRCode
                {
                    UIAlertView alert = new UIAlertView()
                    {
                        Title = "Connection Error",
                        Message = "Unable to connect with server. Please verify your internet connection."
                    };
                    alert.AddButton("OK");
                    alert.Show();

                    Logout();
                }
            }
            catch (Exception) // Unknown user
            {
                UIAlertView alert = new UIAlertView()
                {
                    Title = "Unknown User",
                    Message = "Login to continue."
                };
                alert.AddButton("OK");
                alert.Show();

                Logout();
            }
        }







        // Button Refresh
        void HandleRefresh(object sender, EventArgs e)
        {
            getMessages();
        }


        // Button Logout
        void HandleLogout(object sender, EventArgs e)
        {
            Logout();
        }


        // Logout function
        void Logout()
        {
            var plist = NSUserDefaults.StandardUserDefaults;
            plist.SetString("", "User_ID");
            plist.SetString("", "Username");
            plist.Synchronize();

            UIStoryboard board = UIStoryboard.FromName("Main", null);
            UIViewController ctrl = (UIViewController)board.InstantiateViewController("MainView");
            ctrl.ModalTransitionStyle = UIModalTransitionStyle.CoverVertical;
            this.PresentViewController(ctrl, true, null);
        }




        // Messages
        [Preserve(AllMembers = true)]
        public class Messages
        {
            public int id { get; set; }
            
            public string descricao { get; set; }
            
            public String date { get; set; }
            
            public int user_id { get; set; }
        }

        void getMessages()
        {
            string[] tableItems = new string[] { "No messages to show" };
            string[] tableSubItems = new string[] { "Please refresh to get messages." };

            tvMessages.Source = new TableSource(tableItems, tableSubItems, this);
            Add(tvMessages);
            tvMessages.ReloadData();
            
            try
            {
                // Get Shared User Defaults
                var plist = NSUserDefaults.StandardUserDefaults;
                var user_id = plist.StringForKey("User_ID");
                var username = plist.StringForKey("Username");

                var response = Http.Post("http://192.168.43.59:64001/auth/getTasks", new NameValueCollection() {
                        { "id", user_id },
                        { "username", username }
                    });

                string result = System.Text.Encoding.UTF8.GetString(response);
                
                if (result == "{\"status\":false}" || result.Equals(string.Empty) || response.Equals(string.Empty) || result.Equals("[Array[0]],")) // no messages to show
                {
                    return;
                }

                List<Messages> msgs = JsonConvert.DeserializeObject<List<Messages>>(result);

                tableItems = new string[msgs.Count];
                tableSubItems = new string[msgs.Count];
                var i = 0;
                foreach (var msg in msgs)
                {
                    if (msg.id == -1)
                    {
                        tableItems[i] = "Birthday";
                    }
                    else
                    {
                        tableItems[i] = "Task";
                    }
                    tableSubItems[i] = msg.descricao;
                    i++;
                }
            }
            catch (Exception) // Error getting QRCode
            {
                UIAlertView alert = new UIAlertView()
                {
                    Title = "Connection Error",
                    Message = "Unable to connect with server. Please verify your internet connection."
                };
                alert.AddButton("OK");
                alert.Show();
                return;
            }
            
            tvMessages.Source = new TableSource(tableItems, tableSubItems, this);
            tvMessages.ReloadData();
        }




        // Table Messages
        public class TableSource : UITableViewSource
        {

            string[] TableItems;
            string[] TableSubItems;
            Home owner;
            string CellIdentifier = "TableCell";

            public TableSource(string[] items, string[] subItems, Home owner)
            {
                TableItems = items;
                TableSubItems = subItems;
                this.owner = owner;
            }

            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return TableItems.Length;
            }
            

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                string item = TableItems[indexPath.Row];
                string desc = TableSubItems[indexPath.Row];

                UITableViewCell cell = tableView.DequeueReusableCell(CellIdentifier);
                if (cell == null)
                { cell = new UITableViewCell(UITableViewCellStyle.Subtitle, CellIdentifier); }

                cell.TextLabel.Text = item;
                cell.DetailTextLabel.Text = desc;

                return cell;
            }

            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                UIAlertController okAlertController = UIAlertController.Create(TableItems[indexPath.Row], TableSubItems[indexPath.Row], UIAlertControllerStyle.Alert);
                okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
                owner.PresentViewController(okAlertController, true, null);
                tableView.DeselectRow(indexPath, true);
            }
            
        }
    }
}