﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace SparkleIT.IPVC.YourAccess.Mobile.iOS
{
    [Register ("Home")]
    partial class Home
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem btLogout { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem btRefresh { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ivQrcode { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lbUsername { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView tvMessages { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (btLogout != null) {
                btLogout.Dispose ();
                btLogout = null;
            }

            if (btRefresh != null) {
                btRefresh.Dispose ();
                btRefresh = null;
            }

            if (ivQrcode != null) {
                ivQrcode.Dispose ();
                ivQrcode = null;
            }

            if (lbUsername != null) {
                lbUsername.Dispose ();
                lbUsername = null;
            }

            if (tvMessages != null) {
                tvMessages.Dispose ();
                tvMessages = null;
            }
        }
    }
}