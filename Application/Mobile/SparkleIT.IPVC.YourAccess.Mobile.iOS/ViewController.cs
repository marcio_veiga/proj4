using SparkleIT.IPVC.YourAccess.Mobile.iOS.Helpers;
using System;
using System.Collections.Specialized;
using Foundation;
using UIKit;
using System.Security.Cryptography;
using System.Text;

namespace SparkleIT.IPVC.YourAccess.Mobile.iOS
{
    public partial class ViewController : UIViewController
    {

        public ViewController(IntPtr handle) : base(handle) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            try
            {
                // Get Shared User Defaults
                var plist = NSUserDefaults.StandardUserDefaults;
                var user_id = plist.StringForKey("User_ID");
                var username = plist.StringForKey("Username");

                //if (!(user_id.Equals(string.Empty) || username.Equals(string.Empty)))
                //{ // Loged
                //    goToHome();
                //    return;
                //}
            }
            catch (Exception) {}

            // Hide password
            tbPassword.SecureTextEntry = true;
            
            btLogin.Clicked += HandleClick;
        }


        void HandleClick(object sender, EventArgs e)
        {

            if (tbUsername.Text.Length <= 0)
            {
                UIAlertView alert = new UIAlertView()
                {
                    Title = "Username Required",
                    Message = "Please insert your username."
                };
                alert.AddButton("OK");
                alert.Show();
                return;
            }

            if (tbPassword.Text.Length <= 0)
            {
                UIAlertView alert = new UIAlertView()
                {
                    Title = "Password Required",
                    Message = "Please insert your password."
                };
                alert.AddButton("OK");
                alert.Show();
                return;
            }
            

            try
            {
                var response = Http.Post("http://192.168.43.59:64001/auth/login", new NameValueCollection() {
                    { "username", tbUsername.Text },
                    { "password", tbPassword.Text }
                });

                string result = System.Text.Encoding.UTF8.GetString(response);

                if (result.Equals("-1") || result.Equals("0"))
                {
                    UIAlertView alert = new UIAlertView()
                    {
                        Title = "Login Failed",
                        Message = "Username or password incorrect."
                    };
                    alert.AddButton("OK");
                    alert.Show();
                }
                else
                {
                    var plist = NSUserDefaults.StandardUserDefaults;
                    plist.SetString(result, "User_ID");
                    plist.SetString(tbUsername.Text, "Username");
                    plist.Synchronize();

                    goToHome();
                }
            }
            catch (Exception)
            {
                UIAlertView alert = new UIAlertView()
                {
                    Title = "Connection Error",
                    Message = "Unable to connect with server. Please verify your internet connection."
                };
                alert.AddButton("OK");
                alert.Show();
            }
        }


        public void goToHome()
        {
            UIStoryboard board = UIStoryboard.FromName("Main", null);
            UIViewController ctrl = (UIViewController)board.InstantiateViewController("HomeView");
            ctrl.ModalTransitionStyle = UIModalTransitionStyle.PartialCurl;
            this.PresentViewController(ctrl, true, null);
        }


       

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

