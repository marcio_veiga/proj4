﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace SparkleIT.IPVC.YourAccess.Mobile.iOS
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem btLogin { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField tbPassword { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField tbUsername { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (btLogin != null) {
                btLogin.Dispose ();
                btLogin = null;
            }

            if (tbPassword != null) {
                tbPassword.Dispose ();
                tbPassword = null;
            }

            if (tbUsername != null) {
                tbUsername.Dispose ();
                tbUsername = null;
            }
        }
    }
}