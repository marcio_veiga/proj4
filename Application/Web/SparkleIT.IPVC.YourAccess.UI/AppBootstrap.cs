﻿using Castle.Windsor;
using Microsoft.Owin;
using SparkleIT.IPVC.YourAccess.UI;
using Owin;
using SparkleIT.IPVC.YourAccess.Infrastructure.Web.Container;
using Microsoft.Owin.Cors;

[assembly: OwinStartup(typeof(AppBootstrap))]
namespace SparkleIT.IPVC.YourAccess.UI
{
    public class AppBootstrap
    {
        public static IWindsorContainer Container { get; private set; }
        public void Configuration(IAppBuilder app)
        {
            var container = BootContainer();
            app.UseCors(CorsOptions.AllowAll);
        }

        public static IWindsorContainer BootContainer()
        {
            var bootstrapper = new ContainerBootstrapper();
            Container = bootstrapper.Boot();
            return Container;
        }

        public static void CleanupContainer()
        {
            Container.Dispose();
        }
    }
}
