﻿using System.Configuration;
using System.Web.Optimization;
using SparkleIT.IPVC.YourAccess.Infrastructure.Web;

namespace SparkleIT.IPVC.YourAccess.UI
{

    public class JqueryBundle : JavascriptBundle
    {
        public JqueryBundle()
            : base("~/bundles/jquery")
        {
            AddFiles(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery-ui-{version}.js"
                );
        }
    }

    public class JqueryValidationBundle : JavascriptBundle
    {
        public JqueryValidationBundle()
            : base("~/bundles/jqueryval")
        {
            AddFiles(
                "~/Scripts/jquery.unobtrusive*",
                "~/Scripts/jquery.validate*"
                );
        }
    }

    public class AngularJsBundle : JavascriptBundle
    {
        public AngularJsBundle() : base("~/bundles/angular")
        {
            AddFiles(
               "~/Scripts/angular.js",
                "~/Scripts/angular-animate.js",
                "~/Scripts/angular-sanitize.js",
                "~/Scripts/angular-messages.js",
                "~/Scripts/angular-touch.js",                                
                "~/Scripts/angular-ui-router.js",
                "~/Scripts/angular-ui/ui-bootstrap.js",
                "~/Scripts/angular-ui/ui-bootstrap-tpls.js",
                "~/Scripts/angular-ui/ui-utils.js",
                "~/Scripts/angular-ui/ui-utils-ieshiv.js",
                "~/Scripts/angular-ui/calendar.js",
                "~/Scripts/select.js",                
                "~/Scripts/ngStorage.js",
                "~/Scripts/toaster.js",
                "~/Scripts/moment.min.js",
                "~/Scripts/bootstrap-datetimepicker.js",                
                "~/Scripts/lodash.js",
                "~/Scripts/bootstrap-growl.js",
                "~/Scripts/angular-aria.min.js",
                "~/Scripts/angular-material.min.js",
                "~/Scripts/slimscroll.min.js",
                "~/Scripts/angular-simple-logger.js",
                "~/Scripts/angular-google-maps.min.js",
                "~/Scripts/smart-table.min.js"
                );
        }
    }
    
    public class ModernizrBundle : JavascriptBundle
    {
        public ModernizrBundle() : base("~/bundles/modernizr")
        {
            AddFiles(
                "~/Scripts/modernizr-*"
                );
        }
    }

    public class BootstrapScriptsBundle : JavascriptBundle
    {
        public BootstrapScriptsBundle() : base("~/bundles/bootstrap")
        {
            AddFiles(
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"
                );
        }
    }

    public class ApplicationScriptsBundle : JavascriptBundle
    {
        public ApplicationScriptsBundle() : base("~/bundles/app")
        {
            AddFiles("~/app/app.config.js",
                    "~/app/app.run.js",
                    "~/app/config.js",                    
                    "~/app/config.route.js");
            IncludeDirectory("~/app", "_init_.js", true);
            IncludeDirectory("~/app", "*.js", true);
        }
    }

    public class ProductionApplicationScriptsCompositeBundle : CompositeJavascriptBundle
    {
        public ProductionApplicationScriptsCompositeBundle() : base("~/bundles/js")
        {
            AddFiles(
                new JqueryBundle(),
                new BootstrapScriptsBundle(),
                new AngularJsBundle(),
                new ApplicationScriptsBundle()
                );
            IncludeDirectory("~/app", "_init_.js", true);
            IncludeDirectory("~/app", "*.js", true);
        }

        public void SetupIgnoreList(IgnoreList ignoreList)
        {
            ignoreList.Ignore("*.intellisense.js");
            ignoreList.Ignore("*-vsdoc.js");
            ignoreList.Ignore("*.debug.js");
            ignoreList.Ignore("*.map");
        }
    }

    public class ApplicationStyleBundle : CssBundle
    {
        public ApplicationStyleBundle() : base("~/Content/css")
        {
            AddFiles("~/Content/bootstrap.css",
                "~/Content/bootstrap-theme.css",
                "~/Content/theme/less/skins/_all-skins.css",
                "~/Content/font-awesome.css",
                "~/Content/toaster.css",
                "~/Content/select.css",                
                "~/Content/themes/app.css",                
                "~/Content/app.css",
                "~/Content/bootstrap-datetimepicker.css",
                "~/Content/angular-material.min.css"
                );
        }
    }

    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ModernizrBundle());
            bundles.Add(new JqueryBundle());
            bundles.Add(new BootstrapScriptsBundle());
            bundles.Add(new AngularJsBundle());
            bundles.Add(new ApplicationScriptsBundle());
            bundles.Add(new ApplicationStyleBundle());
        }        
    }


}
