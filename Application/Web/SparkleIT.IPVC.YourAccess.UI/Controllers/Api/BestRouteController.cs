﻿using Newtonsoft.Json;
using SparkleIT.IPVC.YourAccess.Domain.Model;
using SparkleIT.IPVC.YourAccess.Domain.Repository;
using SparkleIT.IPVC.YourAccess.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;



namespace SparkleIT.IPVC.YourAccess.UI.Controllers.Api
{
    class ClientesOrdenadosEnviar
    {
        private decimal latitude;
        private decimal longitude;

        public ClientesOrdenadosEnviar(decimal latitude, decimal longitude)
        {
            this.Latitude = latitude;
            this.Longitude = longitude;

        }

        public decimal Latitude { get => latitude; set => latitude = value; }
        public decimal Longitude { get => longitude; set => longitude = value; }
    }

    class Temp
    {
        private int id1;
        private int id2;

        public int Id1 { get => id1; set => id1 = value; }
        public int Id2 { get => id2; set => id2 = value; }

        public Temp(int id1, int id2)
        {
            this.id1 = id1;
            this.id2 = id2;
        }

    }

    class Distancia
    {
        private int clienteInicio;
        private int clienteFim;
        private double custo;

        public double Custo { get => custo; set => custo = value; }
        public int ClienteInicio { get => clienteInicio; set => clienteInicio = value; }
        public int ClienteFim { get => clienteFim; set => clienteFim = value; }

        public Distancia(double custo, int clienteInicio, int clienteFim)
        {
            this.custo = custo;
            this.clienteInicio = clienteInicio;
            this.clienteFim = clienteFim;

        }
    }

    class Rota
    {
        private string nome;
        private List<Distancia> custos;
        private int numeroClientes;
        private double[,] matrizCustos;

        public string Nome { get => nome; set => nome = value; }
        internal List<Distancia> Custos { get => custos; set => custos = value; }
        public int NumeroClientes { get => numeroClientes; set => numeroClientes = value; }
        public double[,] MatrizCustos { get => matrizCustos; set => matrizCustos = value; }

        public Rota(string nome, List<Distancia> custos, int numeroClientes, double [,]matrizCustos)
        {
            this.nome = nome;
            this.custos = custos;
            this.numeroClientes = numeroClientes;
            this.matrizCustos = matrizCustos;

        }


    }


    [RoutePrefix("api/bestRoute")]
    public class BestRouteController : ApiController
    {

        ClientRepository clientRepository = new ClientRepository();
        TruckRepository truckRepository = new TruckRepository();
        StorageRepository storageRepository = new StorageRepository();

        RouteClientRepository rotasCriadas = new RouteClientRepository();

        //vai conter N rotas
       // List<List<Rota>> rotasTodas = new List<List<Rota>>();

        //vai funcionar como temporario para conter custos de cada rota
        List<Distancia> custosRota;

        //vai conter as rotas para fazer o algoritmo
        List<Rota> rotasAlg;


        //vars para aplicar o algoritmo
        double[,] matrix;
        int[] clientes_visitados;


        private static double custo = 0;
        private static List<Temp> clientesEscolhidos;
        private static List<List<int>> teste;
        private static List<List<int>> clientesOrdenados;


        List<List<ClientsInRoutesDTO>> rotasBD;

        [HttpGet, Route("getall")]
        public IHttpActionResult GetAll()
        {
            try
            {
                //inicialziar Lists
                clientesEscolhidos  = new List<Temp>();
                teste = new List<List<int>>();
                clientesOrdenados = new List<List<int>>();
                rotasBD = new List<List<ClientsInRoutesDTO>>();
                rotasAlg = new List<Rota>();

                //contem as rotas da BD
                var clients = clientRepository.GetClientsInRoutes();

                //contem os armazens da BD
                var storages = storageRepository.GetAll();

                //contem os camioes da BD
                var trucks = truckRepository.GetAll();





                //numeros de rotas e agrupar os clientes para cada uma
                var nRotasBD = clients.GroupBy(a => a.fkRota);

                //dividir os clientes por cada uma das rotas
                foreach (var rotaParticular in nRotasBD)
                {


                    rotasBD.Add(rotaParticular.ToList());


                }

                

                foreach (List<ClientsInRoutesDTO> rotaParticular in rotasBD)
                {

                    rotaParticular.Insert(0, new ClientsInRoutesDTO(0, rotaParticular[0].armazemLat, rotaParticular[0].armazemLon, rotaParticular[0].fkRota, rotaParticular[0].descricao, 0, 0));

                    custosRota = new List<Distancia>();
                    string nomeRota = rotaParticular[0].descricao;
                    matrix = new double[rotaParticular.Count, rotaParticular.Count];

              

                    
                    for (int i = 0; i < rotaParticular.Count(); i++)

                    {
                        for (int j = 0; j < rotaParticular.Count(); j++)
                        {

                            if (j != i)
                            {

                                double distancia = new Coordinates(Convert.ToDouble(rotaParticular[i].latitude), Convert.ToDouble(rotaParticular[i].longitude))
                                .DistanceTo(
                                    new Coordinates(Convert.ToDouble(rotaParticular[j].latitude), Convert.ToDouble(rotaParticular[j].longitude)),
                                    UnitOfLength.Kilometers
                                );

                                custosRota.Add(new Distancia(distancia, Convert.ToInt32(rotaParticular[i].idCliente), Convert.ToInt32(rotaParticular[j].idCliente)));
                                matrix[i, j] = distancia;

                            }
                            else
                            {
                                custosRota.Add(new Distancia(0, Convert.ToInt32(rotaParticular[i].idCliente), Convert.ToInt32(rotaParticular[j].idCliente)));

                                matrix[i, j] = 0;
                            }

                        }

                    }


                    rotasAlg.Add(new Rota(nomeRota, custosRota, rotaParticular.Count(), matrix));



                }



                //LISTAAAR

                System.Diagnostics.Debug.WriteLine("\n");
                System.Diagnostics.Debug.WriteLine("\n");

            
                

                for (int k = 0; k < rotasAlg.Count(); k++)

                    {

                    int numClientes = rotasAlg[k].NumeroClientes;

                    System.Diagnostics.Debug.WriteLine("\n OUTRA ROTA \n");


                    for (int i = 0; i < rotasAlg[k].Custos.Count; i++)
                        {

                        teste.Add(new List<int>());
                        clientesOrdenados.Add(new List<int>());

                        if (i != 0)
                            {
                                if (i % numClientes == 0)
                                {

                                    System.Diagnostics.Debug.WriteLine("\n");

                                    System.Diagnostics.Debug.Write(rotasAlg[k].Custos[i].ClienteInicio.ToString() + " | " + rotasAlg[k].Custos[i].Custo.ToString() +  " | ");
                                
                                teste[k].Add(rotasAlg[k].Custos[i].ClienteInicio);
                                }
                                else
                                {
                                    System.Diagnostics.Debug.Write(rotasAlg[k].Custos[i].Custo.ToString() + " | ");

                                }
                            }
                            else
                            {

                                System.Diagnostics.Debug.Write(rotasAlg[k].Custos[i].ClienteInicio.ToString() + " | " + rotasAlg[k].Custos[i].Custo.ToString() +  " | ");

                            teste[k].Add(rotasAlg[k].Custos[i].ClienteInicio);
                        }



                        }

                    }
                System.Diagnostics.Debug.WriteLine("\n");
                System.Diagnostics.Debug.WriteLine("\n");


                //aplicar algoritmo

                List<int> temp = new List<int>();

                for (int k= 0; k < rotasAlg.Count; k++)
                {
                    clientes_visitados = new int[rotasAlg[k].Custos.Count];
                    clientesEscolhidos = new List<Temp>();



                    System.Diagnostics.Debug.Write("\n Caminho da rota - " + rotasAlg[k].Nome + " ");

                    minimum_cost(0, clientes_visitados, rotasAlg[k].NumeroClientes, rotasAlg[k].MatrizCustos, k);

                    //reset variavel
                    custo = 0;

                    for (int i = 0; i < clientes_visitados.Length; i++) // Using for loop we are initializing
                    {
                        clientes_visitados[i] = 0;
                    }
                   
                }


                int x = 0;
                List<ClientesOrdenadosEnviar> clientesOrdenadosCoords = new List<ClientesOrdenadosEnviar>();
                List<List<ClientesOrdenadosEnviar>> rotaClientesOrdenadosCoords = new List<List<ClientesOrdenadosEnviar>>();
                foreach (List<int> aux in clientesOrdenados)
                {
                    if (aux.Count == 0)
                    {



                    }
                    else
                    {

                        System.Diagnostics.Debug.WriteLine("\nRota ordenada: " + x + "\n");

                     //   rotaClientesOrdenadosCoords.Add(new List<ClientesOrdenadosEnviar>());
                            

                        x++;
                        //depois de termos cada uma das rotas ordenadas vamos contruir o JSON e enviar para a VIEW(JSON em formato para o angular-google-maps reconhecer polyline)
                        foreach (int aux2 in aux)
                        {
                            foreach (List<ClientsInRoutesDTO> rotaParticular in rotasBD)
                            {
                                for(int i =0; i< rotaParticular.Count; i++)
                                {
                                    if(rotaParticular[i].idCliente == aux2)
                                    {
                                        clientesOrdenadosCoords.Add(new ClientesOrdenadosEnviar(rotaParticular[i].latitude, rotaParticular[i].longitude));

                                        System.Diagnostics.Debug.Write(" " + rotaParticular[i].idCliente + " \n");
                                        System.Diagnostics.Debug.Write(" " + rotaParticular[i].latitude + " \n");
                                        System.Diagnostics.Debug.Write(" " + rotaParticular[i].longitude + " \n");

                                    }
                                }


                            }
                            
                        }
                        rotaClientesOrdenadosCoords.Add(new List<ClientesOrdenadosEnviar>(clientesOrdenadosCoords));

                        clientesOrdenadosCoords.Clear();
                    }
                }
             //   System.Diagnostics.Debug.Write("teste" + clientesOrdenadosCoords[0].ToString());

              //  var json = JsonConvert.SerializeObject(clientesOrdenadosCoords);
                

                return Ok(rotaClientesOrdenadosCoords);


            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }




            

        }

        



        /*
 * 
 * Algoritmo TSP
 * 
 * 
 */
        public static void minimum_cost(int city, int[] clientes_visitados, int nCidades, double[,] matrix, int k)
        {


            int nearest_city;
            int verificaNovaRota = 0, rotaAnterior = 0;

            clientes_visitados[city] = 1;

            System.Diagnostics.Debug.Write(city + 1 + " ");

            clientesOrdenados[k].Add(teste[k][city]);

            nearest_city = tsp(city, nCidades, clientes_visitados, matrix);

            

            //calcula para voltar a origem
            if (nearest_city == 999)
            {

                nearest_city = 0;

                System.Diagnostics.Debug.Write(nearest_city + 1);


                 custo += matrix[city, nearest_city];

                return;
            }

            minimum_cost(nearest_city, clientes_visitados, nCidades, matrix, k);
        }

        public static int tsp(int c, int nCidades, int[] clientes_visitados, double[,] matrix)
        {

            int count, nearest_city = 999;
            double minimum = 999, temp = 0;

            for (count = 0; count < nCidades; count++)
            {

                if ((matrix[c, count] != 0) && (clientes_visitados[count] == 0))
                {

                    if (matrix[c, count] < minimum)
                    {

                        minimum = matrix[count, 0] + matrix[c, count];

                    }

                    temp = matrix[c, count];
                    
                    nearest_city = count;


                }
            }

            if (minimum != 999)
            {
                custo += temp;
            }

            return nearest_city;
        }

 


    }


}
