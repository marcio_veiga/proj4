﻿using SparkleIT.IPVC.YourAccess.Domain.Model;
using SparkleIT.IPVC.YourAccess.Domain.Repository;
using SparkleIT.IPVC.YourAccess.UI.ViewModels;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;


namespace SparkleIT.IPVC.YourAccess.UI.Controllers.Api
{
    [RoutePrefix("api/client")]  
    public class ClientController: ApiController 
    {
        ClientRepository _repository = new ClientRepository();

        [HttpGet, Route("getall")]
        public IHttpActionResult GetAll()
        {
            try
            {
                var clients = _repository.GetAll();

                return Ok(clients);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpGet, Route("getAllClientsInRoutes")]
        public IHttpActionResult GetAllClientsInRoutes()
        {
            try
            {
                var clients = _repository.GetClientsInRoutes();

                return Ok(clients);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        [HttpGet, Route("getbyid/{id:int}")]
        public IHttpActionResult GetById(int id)
        {
            try
            {
                var client = _repository.GetById(id);

                return Ok(client);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }




        [HttpPost, Route("save")]
        public IHttpActionResult Save(ClientViewModel model)
        {
            try
            {
                Client client = new Client { nome = model.nome, morada = model.morada, latitude = model.latitude, longitude = model.longitude, ativo = model.ativo, estado = model.estado};


                return Ok(_repository.Save(client));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
    }
}