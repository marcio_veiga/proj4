﻿using SparkleIT.IPVC.YourAccess.Domain.Model;
using SparkleIT.IPVC.YourAccess.Domain.Repository;
using SparkleIT.IPVC.YourAccess.UI.ViewModels;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;


namespace SparkleIT.IPVC.YourAccess.UI.Controllers.Api
{
    [RoutePrefix("api/routeClient")]  
    public class RouteClientClientController: ApiController 
    {
        RouteClientRepository _repository = new RouteClientRepository();

        [HttpGet, Route("getall")]
        public IHttpActionResult GetAll()
        {
            try
            {
                var routeClients = _repository.GetAll();

                return Ok(routeClients);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        [HttpGet, Route("getbyid/{id:int}")]
        public IHttpActionResult GetById(int id)
        {
            try
            {
                var routeClient = _repository.GetById(id);

                return Ok(routeClient);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }




        [HttpPost, Route("save")]
        public IHttpActionResult Save(RouteClientViewModel model)
        {
            try
            {
                RouteClient routeClient = new RouteClient {  fkRota = model.fkRota, fkCliente = model.fkCliente };


                return Ok(_repository.Save(routeClient));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
    }
}