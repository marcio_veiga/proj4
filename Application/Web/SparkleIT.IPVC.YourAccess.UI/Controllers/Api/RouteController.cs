﻿using SparkleIT.IPVC.YourAccess.Domain.Model;
using SparkleIT.IPVC.YourAccess.Domain.Repository;
using SparkleIT.IPVC.YourAccess.UI.ViewModels;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;


namespace SparkleIT.IPVC.YourAccess.UI.Controllers.Api
{
    [RoutePrefix("api/route")]  
    public class RouteController: ApiController 
    {

        RouteRepository _repository = new RouteRepository();

        [HttpGet, Route("getall")]
        public IHttpActionResult GetAll()
        {
            try
            {
                var routes = _repository.GetAll();

                return Ok(routes);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        [HttpGet, Route("getbyid/{id:int}")]
        public IHttpActionResult GetById(int id)
        {
            try
            {
                var route = _repository.GetById(id);

                return Ok(route);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }




        [HttpPost, Route("save")]
        public IHttpActionResult Save(RouteViewModel model)
        {
            try
            {
                Route route = new Route { descricao = model.descricao, fkArmazem = model.fkArmazem, fkCamiao = model.fkCamiao, ativo = model.ativo, estado = model.estado };


                return Ok(_repository.Save(route));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
    }
}