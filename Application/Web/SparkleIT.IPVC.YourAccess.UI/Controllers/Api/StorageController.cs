﻿using SparkleIT.IPVC.YourAccess.Domain.Model;
using SparkleIT.IPVC.YourAccess.Domain.Repository;
using SparkleIT.IPVC.YourAccess.UI.ViewModels;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;


namespace SparkleIT.IPVC.YourAccess.UI.Controllers.Api
{
    [RoutePrefix("api/storage")]  
    public class StorageController: ApiController 
    {
        StorageRepository _repository = new StorageRepository();

        [HttpGet, Route("getall")]
        public IHttpActionResult GetAll()
        {
            try
            {
                var storages = _repository.GetAll();

                return Ok(storages);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        [HttpGet, Route("getbyid/{id:int}")]
        public IHttpActionResult GetById(int id)
        {
            try
            {
                var storage = _repository.GetById(id);

                return Ok(storage);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }




        [HttpPost, Route("save")]
        public IHttpActionResult Save(StorageViewModel model)
        {
            try
            {
                Storage storage = new Storage { nome = model.nome, morada = model.morada, latitude = model.latitude, longitude = model.longitude, ativo = model.ativo};


                return Ok(_repository.Save(storage));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
    }
}