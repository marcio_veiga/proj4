﻿using SparkleIT.IPVC.YourAccess.Domain.Model;
using SparkleIT.IPVC.YourAccess.Domain.Repository;
using SparkleIT.IPVC.YourAccess.UI.ViewModels;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;


namespace SparkleIT.IPVC.YourAccess.UI.Controllers.Api
{
    [RoutePrefix("api/truck")]  
    public class TruckController: ApiController 
    {
        TruckRepository _repository = new TruckRepository();

        [HttpGet, Route("getall")]
        public IHttpActionResult GetAll()
        {
            try
            {
                var trucks = _repository.GetAll();

                return Ok(trucks);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        [HttpGet, Route("getbyid/{id:int}")]
        public IHttpActionResult GetById(int id)
        {
            try
            {
                var truck = _repository.GetById(id);

                return Ok(truck);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        [HttpPost, Route("save")]
        public IHttpActionResult Save(TruckViewModel model)
        {
            try
            {
                Truck truck = new Truck { nome = model.nome, estado = model.estado, ativo = model.ativo };


                return Ok(_repository.Save(truck));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
    }
}