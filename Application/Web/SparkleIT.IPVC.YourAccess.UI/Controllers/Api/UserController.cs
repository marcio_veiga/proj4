﻿using SparkleIT.IPVC.YourAccess.Domain.Model;
using SparkleIT.IPVC.YourAccess.Domain.Repository;
using SparkleIT.IPVC.YourAccess.UI.ViewModels;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;


namespace SparkleIT.IPVC.YourAccess.UI.Controllers.Api
{
    [RoutePrefix("api/user")]  
    public class UserController: ApiController 
    {
        UserRepository _repository = new UserRepository();

        [HttpGet, Route("getall")]
        public IHttpActionResult GetAll()
        {
            try
            {
                var users = _repository.GetAll();

                return Ok(users);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        [HttpGet, Route("getbyid/{id:int}")]
        public IHttpActionResult GetById(int id)
        {
            try
            {
                var user = _repository.GetById(id);

                return Ok(user);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        [HttpPost, Route("try-login")]
        public IHttpActionResult TryLogin(LoginViewModel model)
        {
            try
            {
                var user = _repository.GetAuth(model.Username, GetMd5(MD5.Create(), model.Password));

               
                return Ok(user);
              
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


        }


        static string GetMd5(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }




        [HttpPost, Route("save")]
        public IHttpActionResult Save(UserViewModel model)
        {
            try
            {
                User user = new User { name = model.name, username = model.username, password = model.password, ativo = model.ativo};


                return Ok(_repository.Save(user));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
    }
}