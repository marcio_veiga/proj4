﻿
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using NLog;
using SparkleIT.IPVC.YourAccess.Infrastructure.Web;

namespace SparkleIT.IPVC.YourAccess.UI
{
    public class YourAccessApplication : MvcApplication
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        protected override void OnApplicationStart()
        {
            base.OnApplicationStart();

            SetupDefaultIgnoreRoutes(RouteTable.Routes);
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected override void OnEndApplication()
        {
            base.OnEndApplication();
        }
    }
}
