﻿using System;

namespace SparkleIT.IPVC.YourAccess.UI.ViewModels
{
    public class ClientViewModel
    {
        public string nome { get; set; }
        public string morada { get; set; }
        public decimal latitude { get; set; }
        public decimal longitude { get; set; }
        public int ativo { get; set; }
        public int estado { get; set; }


    }
}