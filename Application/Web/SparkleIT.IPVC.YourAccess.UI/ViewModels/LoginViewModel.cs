﻿using SparkleIT.IPVC.YourAccess.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

namespace SparkleIT.IPVC.YourAccess.UI.ViewModels
{
   
    public class LoginViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}