﻿using System;

namespace SparkleIT.IPVC.YourAccess.UI.ViewModels
{
    public class RouteViewModel
    {
        public string descricao { get; set; }
        public long fkArmazem { get; set; }
        public long fkCamiao { get; set; }
        public int ativo { get; set; }
        public int estado { get; set; }


    }
}