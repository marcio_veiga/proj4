﻿using System;

namespace SparkleIT.IPVC.YourAccess.UI.ViewModels
{
    public class UserViewModel
    {
        public string name { get; set; }
        public string email { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public int ativo { get; set; }

    }
}