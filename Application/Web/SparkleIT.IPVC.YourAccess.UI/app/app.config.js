﻿var SparkleIT = SparkleIT || {};
SparkleIT.IPVC = SparkleIT.YourAccess || {};

SparkleIT.YourAccess = angular.module('SparkleIT.YourAccess', [
    'ngAnimate',
    'ngSanitize',
    'ngMessages',
    'ngStorage',    
    'ui.router',
    'ui.bootstrap',
    'ui.utils',    
    'ui.select',    
    'toaster',
    'ngMaterial',
    'ngAria',
    'uiGmapgoogle-maps',
    'smart-table'
]);
