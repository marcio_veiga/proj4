﻿(function (app) {
    'use strict';

    app.run(['$rootScope', 'AuthService', '$state', '$mdDialog', function ($rootScope, AuthService, $state, $mdDialog) {


        $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {

            var requireLogin = toState.data ? toState.data.requireLogin : false;
            var ROLES = toState.data ? toState.data.roles : [];

            if (requireLogin) {
                if (!AuthService.isAuthenticated()) {
                    notLogged(event);
                    return false;
                }

                if (!AuthService.isAutorized(ROLES)) {

                    notAutorized(event);
                };

                $rootScope.$broadcast('$logged');
            }
        });


        $rootScope.showAlert = function (title, msg) {
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title(title)
                .textContent(msg)
                .ok('OK')
            );
        };


        function notLogged(event) {
            event.preventDefault();
            $rootScope.showAlert("Error", "Please login before continue.");
            $state.go('app.login');
        }


        function notAutorized(event) {
            event.preventDefault();

            $rootScope.showAlert("Error", "Access Denied.");
        }

    }]);

})(SparkleIT.YourAccess);