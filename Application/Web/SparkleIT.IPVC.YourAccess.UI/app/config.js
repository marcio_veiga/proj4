﻿(function(app) {
    'use strict';

    app.config([

        '$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(false).hashPrefix('!');
        }

    ]);


    app.config([

        'uiGmapGoogleMapApiProvider', function (uiGmapGoogleMapApiProvider) {
            uiGmapGoogleMapApiProvider.configure({
                key: 'AIzaSyAJZKm6et_IZOtBOj6n5RB1_JyKa-Pv0QU',
                v: '3', 
                libraries: 'weather,geometry,visualization'
            });

        }

    ]);

   app.service('config', function () {

       var config = {

           BuidlUrl: function (url) {
               return 'http://localhost:64001/' + url;
           },

           timeFormat: 'HH:mm',
           dateFormat: 'dd/MM/yyyy',
           dateTimeFormat: 'dd/MM/yyyy HH:mm'
       }
       
       return config;

   });

})(SparkleIT.YourAccess);