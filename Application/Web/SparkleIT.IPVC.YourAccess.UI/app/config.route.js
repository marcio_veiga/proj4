﻿(function(app) {
    'use strict';

    app.constant('states', getStates());

    app.config(StateConfigurator);

    StateConfigurator.$inject = ['$stateProvider', '$urlRouterProvider', '$urlMatcherFactoryProvider', 'states'];

    function StateConfigurator($stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider, states) {

        $urlMatcherFactoryProvider.caseInsensitive(true);
        $urlMatcherFactoryProvider.strictMode(false);

        $urlRouterProvider.otherwise('/login');

        $stateProvider.state('app', {
            abstract: true,
            templateUrl: 'app/shell/shell.html',
        });

        states.forEach(function (s) {
            $stateProvider.state(s.state, s.config);
        });
    }

    function getStates() {
        return [
        ];
    }
})(SparkleIT.YourAccess);

