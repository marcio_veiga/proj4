﻿(function(app) {
    'use strict';

    app.config(RegisterStates);

    RegisterStates.$inject = ['$stateProvider'];

    function RegisterStates($stateProvider) {

        var moduleStates = getModuleStates();
        moduleStates.forEach(function (s) {            
            $stateProvider.state(s.state, s.config);
        });
    };

    function getModuleStates() {
        return [            
            {
                state: 'app.home',
                config: {
                    url: '/home',
                    templateUrl: 'app/home/home.html',
                    controller: 'HomeController as vm',
                    data: {
                        requireLogin: true,
                        roles: [1, 2, 3]
                    }
                }
                
            }];
    };

})(SparkleIT.YourAccess);

