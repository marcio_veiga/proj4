﻿(function (app) {
    'use strict';

    var controllerId = 'HomeController';
    app.controller(controllerId, HomeController);

    HomeController.$inject = ['$state', 'HomeService', 'config', '$filter', '$timeout', '$mdDialog', '$scope', '$modal', 'ClientService', '$log', 'StorageService', 'TruckService', 'uiGmapGoogleMapApi', 'uiGmapIsReady'];

    function HomeController($state, HomeService, config, $filter, $timeout, $mdDialog, $scope, $modal, ClientService, $log, StorageService, TruckService, uiGmapGoogleMapApi, uiGmapIsReady) {

        var vm = this;



        var actualLat;
        var actualLon;
        var i = 0;
        var x = 0;


        vm.title = "A";
        vm.color = "black";
        vm.colors = ["black", "maroon", "red", "orange", "yellow", "lime", "green", "olive", "aqua", "blue", "navy", "purple", "fuchsia", "gray"];

        vm.clients = []; 
        vm.trucks = [];
        vm.storages = [];   
        vm.randomMarkers = [];
        vm.map = null;
        vm.clientesEmRotas = [];

        vm.polylines = [];

        vm.clientesFinaisRotas = [];

        //caso seja falso mostramos o botao para associar
        vm.estaAssociadoArota;


        //routes directions
   
        vm.instanceMap = null;
        vm.directionsService = null;


        uiGmapIsReady.promise().then(function (maps) {
            //if we initialize directionsDisplay in every call, then we will have the issue with the previous route not cleared.
            // use this and the logic below to setMap to null and directionsDisplay to null.

            return uiGmapIsReady.promise(1);

        }).then(function (instances) {

            vm.instanceMap = instances[0].map;

            vm.directionsService = new google.maps.DirectionsService();


                //criar rotas entre os clientes
                ClientService.getAllClientsInRoutes().success(vm.gerarRotas).error(vm.erroGerarRotas);






            });


        //inicializar o mapa
        uiGmapGoogleMapApi.then(function (maps) {
        

            //definicoes do mapa
            vm.map = {
                center: { latitude: 45, longitude: -73 },
                zoom: 8,

                events: {
                    click: function (map, eventName, originalEventArgs) {
                        var e = originalEventArgs[0];
                        actualLat = e.latLng.lat(), actualLon = e.latLng.lng();

                        vm.showChoiceAlert("Menu", "escolha uma opção", "Novo Cliente", "Novo Armazém");

                    }
                }
            };

            //definiçoes das windows(labels dos markers)
            vm.windowOptions = {
                boxClass: "infobox",
                boxStyle: {
                    backgroundColor: "white",
                    border: "1px solid black",
                    borderRadius: "5px",
                    width: "auto",
                    height: "auto",
                    padding: "10px"
                },

                disableAutoPan: true,
                maxWidth: 0,

                zIndex: null,


                isHidden: false,
                pane: "floatPane",
                enableEventPropagation: false
            };


            //executamos aqui dentro para garantir que adicionamos os markers depois de o mapa estar inicializado

            //Armazens
            StorageService.getAllstorages().success(function (data, status) {
               
                vm.storages = data;

                angular.forEach(data, function (value, key) {

                    var marker = {
                        id: i,
                        latitude: value.latitude,
                        longitude: value.longitude,
                        title: value.nome,
                        content: value.morada,
                        icon: '../img/warehouse01.png',
                    };

                    vm.randomMarkers.push(marker);

                    i++;
                });


            }).error(function (data, status) {
                alert(error);
            });

            //Camioes
            TruckService.getAlltrucks().success(function (data, status) {

                vm.trucks = data;



            }).error(function (data, status) {
                alert(error);
                });


            //Clientes
            ClientService.getAllclients().success(function (data, status) {

                vm.clients = data;

                angular.forEach(data, function (value, key) {


                    var marker = {
                        id: i,
                        latitude: value.latitude,
                        longitude: value.longitude,
                        title: value.nome,
                        content: value.morada,
                        icon: '../img/client01.png',
                    };
                    vm.randomMarkers.push(marker);
                    i++;
                });


            }).error(function (data, status) {
                alert(error);
                });


            vm.botaoMapNovoCliente = function () {
                $log.log("resultou");
            }

            //gerar uma cor random
            vm.getRandomColor = function () {
                return {
                    color: '#' + Math.floor(Math.random() * 16777215).toString(16)
                }
            };

            vm.gerarRotas = function (data, status) {
                vm.clientesEmRotas = data;
                angular.forEach(data, function (value, key) {



                    vm.clientesFinaisRotas.push(value[value.length - 1]);

                    var generatedColor = vm.getRandomColor();

                    var polylineOptionsActual = {
                        strokeColor: generatedColor.color,
                        strokeOpacity: 1.0,
                        strokeWeight: 10
                    };

                    $log.log(polylineOptionsActual);
                    
             
                    var directionsDisplay = new google.maps.DirectionsRenderer({ polylineOptions: polylineOptionsActual });
                    directionsDisplay.setMap(vm.instanceMap);

                    DisplayDrivingDirections(value, $scope, directionsDisplay);
                    /*

                    var generatedColor = vm.getRandomColor();

                    var temp = {
                        id: x,
                        path: value,
                        stroke: {
                            color: generatedColor.color,
                            weight: 5
                        },
                        geodesic: true,
                        visible: true,
                    }

                    vm.polylines.push(temp);

                    x++;
                    */
                });
                    

            }

            vm.erroGerarRotas = function (data, status) {
                alert(error);
            }



        });


        vm.showAlert = function (title, msg) {
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title(title)
                .textContent(msg)
                .ok('OK')
            );
        };

        vm.showChoiceAlert = function (title, msg, ok, cancel) {
            $mdDialog.show(
                $mdDialog.confirm()
                    .title(title)
                    .textContent(msg)
                    .ariaLabel('Lucky day')
                    .ok(ok)
                    .cancel(cancel)
            ).then(function() {

                var template = "modalNovoCliente.html"

                vm.open(template, criarClienteController);

                }, function () {

                var template = "modalNovoArmazem.html"

                vm.open(template, criarArmazemController);
    });
        };
 

        vm.onClick = function (marker, eventName, model) {


            console.log("Clicked!");

            model.show = !model.show;
            vm.activeModel = model;

            $log.log(vm.activeModel);

            vm.windowParams = {
                title: 'TESTE',
                content: 'TESTE2'
            }

            vm.estaAssociadoArota = true;

            $log.log(vm.clientesEmRotas);

            angular.forEach(vm.clientesEmRotas, function (value, key) {

                angular.forEach(value, function (value2, key2) {

                    if (vm.activeModel.latitude == value2.latitude && vm.activeModel.longitude == value2.longitude) {
                        $log.log("esta em rota");
                        vm.estaAssociadoArota = false;

                    }

                });

            });

        };

        vm.open = function (template, controllerDefinido) {

            if (controllerDefinido == "criarRotaController") {
         
                controllerDefinido = criarRotaController;
            }

            $modal.open({
                templateUrl: template, // loads the template
                backdrop: true, // setting backdrop allows us to close the modal window on clicking outside the modal window
                windowClass: 'modal', // windowClass - additional CSS class(es) to be added to a modal window template
                controller: controllerDefinido,
                resolve: {
                    user: function () {
                        return $scope.user;
                    }
                }
            });//end of modal.open
        }; // end of scope.open function


        function criarClienteController($scope, $modalInstance, $log, user, ClientService, RouteClientService) {

            $scope.user = {
                latitude: 0,
                longitude: 0,
                morada: null,
                nome: null
            }


            var latlng = new google.maps.LatLng(actualLat, actualLon);

            geocodeCoordsAddress(latlng, function (address) {
                console.log("entrou");
                console.log(address);
                $scope.user.morada = address;
                
            });

            $scope.submit = function () {
                $log.log('Submiting user info.'); // kinda console logs this statement

                $log.log($scope.user);
                $scope.user.latitude = actualLat;
                $scope.user.longitude = actualLon;
                $scope.user.ativo = 1;
                $scope.user.estado = 0;

                $modalInstance.dismiss('cancel');

                ClientService.saveClient($scope.user).success(function (data, status) {
                   
                    if (data) {
                        vm.showAlert("success", "client successfully created.");

                        var marker = {
                            id: i,
                            latitude: actualLat,
                            longitude: actualLon,
                            title: $scope.user.nome,
                            content: $scope.user.morada,
                             icon: '../img/client01.png'

                        };
                        i++;
                        vm.randomMarkers.push(marker);
                        console.log(vm.randomMarkers);

                        vm.clients.push($scope.user);

                    }
                    else {
                        vm.showAlert("error", "error creating client.");
                    }
                }).error(function (data, status) {
                    vm.showAlert("error", "error creating client.");
                });
            }
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }

        function criarArmazemController($scope, $modalInstance, $log, user, TruckService) {

            $scope.submit = function () {
                $log.log('Submiting user info.'); // kinda console logs this statement

                $log.log($scope.user);
                $scope.user.latitude = actualLat;
                $scope.user.longitude = actualLon;
                $scope.user.ativo = 1;

                var latlng = new google.maps.LatLng(actualLat, actualLon);

                geocodeCoordsAddress(latlng, function (address) {
                    console.log("entrou");
                    console.log(address);
                    $scope.user.morada = address;

                });


                StorageService.saveStorage($scope.user).success(function (data, status) {

                    if (data) {
                        vm.showAlert("success", "storage successfully created.");

                        var marker = {
                            id: i,
                            latitude: actualLat,
                            longitude: actualLon,
                            title: $scope.user.nome,
                            content: null,
                            icon: '../img/warehouse01.png'

                        };
                        i++;
                        vm.randomMarkers.push(marker);
                        console.log(vm.randomMarkers);

                        vm.storages.push($scope.user);

                    }
                    else {
                        vm.showAlert("error", "error creating client.");
                    }
                }).error(function (data, status) {
                    vm.showAlert("error", "error creating client.");
                });


                $modalInstance.dismiss('cancel');

            }
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }

        function criarRotaController($scope, $modalInstance, $log, user, RouteService, RouteClientService, $q) {      

            $log.log(vm.trucks);

            $scope.trucks = [];
            $scope.storages = [];
            $scope.itemArray = [];

            angular.forEach(vm.trucks, function (value, key) {

                if (value.ativo == 1 && value.estado == 0) {
                    console.log("camiao valido");

                    $scope.trucks.push(value);
                }

            });

            angular.forEach(vm.clients, function (value, key) {

                if (value.ativo == 1 && value.estado == 0) {
                    console.log("cliente valido");

                    $scope.itemArray.push(value);
                }

            });

            angular.forEach(vm.storages, function (value, key) {

                if (value.ativo == 1) {
                    console.log("armazem valido");

                    $scope.storages.push(value);
                }

            });


            $scope.selected = { value: null };
            $scope.truckSelected = { value: null };
            $scope.storageSelected = { value: null };

            $scope.routeClient = {
                fkRota : null,
                fkCliente : null
            }

            $scope.submit = function () {

                $log.log('Submiting user info.'); // kinda console logs this statement

                $scope.user.fkArmazem = $scope.storageSelected.value;
                $scope.user.fkCamiao = $scope.truckSelected.value;

                $log.log($scope.selected.value);

                // Define the initial promise
                var sequence = $q.defer();
                sequence.resolve();
                sequence = sequence.promise;
                
                RouteService.saveRoute($scope.user).success(function (data, status) {

                    if (data != -1) {

                        //vm.showAlert("success", "client successfully created.");
                        console.log("criado rota" + data);
                        $scope.routeClient.fkRota = data;

                        //temporario para a nova rota
                        $scope.novaRotaPath = [];

                        angular.forEach($scope.selected.value, function (value, key) {

                            sequence = sequence.then(function () {

                                $scope.novaRotaPath.push({
                                    latitude: value.latitude,
                                    longitude: value.longitude
                                })

                                return doSomething(value);
                            });

                        });

                    }
                    else {
                        vm.showAlert("error", "error creating client.");
                    }
                }).error(function (data, status) {
                    vm.showAlert("error", "error creating client.");
                    });

                function doSomething(value) {

                    $scope.routeClient.fkCliente = value.idCliente;
                  

                    RouteClientService.saveRouteClient($scope.routeClient).success(function (data, status) {

                   

                        if (data) {

                            //vm.showAlert("success", "client successfully created.");
                            console.log("sucesso");

                            //adicionamos ao mapa

                            var generatedColor = vm.getRandomColor();

                            var temp = {
                                id: x,
                                path: $scope.novaRotaPath,
                                stroke: {
                                    color: generatedColor.color,
                                    weight: 5
                                },
                                geodesic: true,
                                visible: true,
                            }

                            vm.polylines.push(temp);

                            x++;


                        }
                        else {
                            vm.showAlert("error", "error creating client.");
                        }
                    }).error(function (data, status) {
                        vm.showAlert("error", "error creating client.");
                    });

                }
              
                $modalInstance.dismiss('cancel');

            }
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }




        
  

    // GEOCODE MORADAS PARA COORDENADAS
    
    var geocodeAddressCoords = function (address, callback) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                callback(results[0].geometry.location);
            } else {
                console.log("Geocode was not successful for the following reason: " + status);
            }
        });
    };


    
        //REVERSE GEOCODE
 
        var geocodeCoordsAddress = function (latlng, callback) {

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                // Do something with result
                $log.log(results);

                if (status === google.maps.GeocoderStatus.OK) {

                    if (results[1]) {
 
                        callback(results[0].formatted_address);

                    } else {
                        console.log('Location not found');
                    }

                } else {
                    console.log('Geocoder failed due to: ' + status);
                }

            });
        
    }

        function DisplayDrivingDirections(path, $scope, directionsDisplay) {

     
            

            if (path !== undefined && path !== undefined) {

                var directionsServiceRequest = {
                    origin: path[0].latitude + "," + path[0].longitude,
                    destination: path[path.length - 1].latitude + "," + path[path.length-1].longitude,
                    waypoints: [],
                    optimizeWaypoints: false,
                    provideRouteAlternatives: false,
                    travelMode: google.maps.TravelMode.DRIVING,
                    unitSystem: google.maps.UnitSystem.METRIC
                };

                
                if (path !== undefined) {

                    for (var y = 0; y < path.length; y++) {

                        if (y == 0 || y == path.length - 1) {

                        } else {
                            console.log("entra intermediario");
                            directionsServiceRequest.waypoints.push({ location: path[y].latitude + "," + path[y].longitude, stopover: true });

                        }

                    }

                }
                
                
                var directions =
                    {
                        TotalDistance: 0,
                        TotalDuration: {},
                        Segments: []
                    };
                vm.directionsService.route(directionsServiceRequest, function (response, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setOptions({ suppressMarkers: true });
                        directionsDisplay.setDirections(response);

                        directions.TotalDistance = 0;
                        directions.Segments = [];
                        var totalDuration = 0;

                        var route = response.routes[0];
                        // For each route, display summary information.
                        for (var i = 0; i < route.legs.length; i++) {
                            var routeSegment = i + 1;

                            var legDistance = route.legs[i].distance.value; // distance in meters need to devide by 1000 for Km
                            directions.TotalDistance = directions.TotalDistance + legDistance / 1000;

                            var legDuration = route.legs[i].duration.value; // travel time in seconds
                            totalDuration = totalDuration + legDuration;

                            directions.Segments.push({
                                Start: route.legs[i].start_address,
                                End: route.legs[i].end_address,
                                Travel: route.legs[i].distance.text,
                                Duration: route.legs[i].duration.text
                            });
                        }

                        //directions.TotalDuration = secondsToTime(totalDuration);

                        directions.TotalDuration = totalDuration;
                        vm.directions = directions;
                    }
                });

            }
            
        }









   
        
    

    };
})(SparkleIT.YourAccess);