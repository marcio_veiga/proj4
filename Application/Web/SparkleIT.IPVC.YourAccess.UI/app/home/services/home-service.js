﻿(function(app) {
    'use strict';

    
    app.factory('HomeService', function ($http, config) {


        var factory = {
            getCalendar: getCalendar,
            save: save,
            removeCalendar: removeCalendar,
            updateCalendar: updateCalendar,
            getDefaultEvents: getDefaultEvents,
            saveEvent: saveEvent,
            removeEvent: removeEvent
        };

        function getCalendar() {
            return $http.get(config.BuidlUrl('api/calendar/getall'));
        };

        function save(obj) {
            return $http.post(config.BuidlUrl('api/calendar/save'), obj);
        };

        function removeCalendar(id) {
            return $http.get(config.BuidlUrl('api/calendar/delete/' + id));
        };

        function updateCalendar(obj) {
            return $http.post(config.BuidlUrl('api/calendar/update'), obj);
        };

        function getDefaultEvents() {
            return $http.get(config.BuidlUrl('api/event/getall'));
        };

        function saveEvent(obj) {
            return $http.post(config.BuidlUrl('api/event/save'), obj);
        };

        function removeEvent(id) {
            return $http.get(config.BuidlUrl('api/event/delete/' + id));
        };


        return factory;
    });


})(SparkleIT.YourAccess);

