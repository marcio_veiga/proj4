﻿(function(app) {
    'use strict';

    app.config(RegisterStates);

    RegisterStates.$inject = ['$stateProvider'];

    function RegisterStates($stateProvider) {

        var moduleStates = getModuleStates();
        moduleStates.forEach(function (s) {            
            $stateProvider.state(s.state, s.config);
        });
    };

    function getModuleStates() {
        return [            
            {
                state: 'app.login',
                config: {
                    url: '/login',
                    templateUrl: 'app/login/login.html',
                    controller: 'LoginController as vm'
                },
                data: {
                        requireLogin: false
                }
            },
            {
                state: 'app.logout',
                config: {
                    url: '/logout',
                    controller: 'LogoutController as vm'
                },
                data: {
                    requireLogin: false
                }
            }


        ];
    };

})(SparkleIT.YourAccess);

