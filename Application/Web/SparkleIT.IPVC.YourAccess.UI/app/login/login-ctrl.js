﻿(function(app) {
    'use strict';

    var controllerId = 'LoginController';
    app.controller(controllerId, LoginController);

    LoginController.$inject = ['$state', 'AuthService', '$mdDialog'];

    function LoginController($state, AuthService, $mdDialog) {

        var vm = this;
        vm.obj = {};
   
        vm.showAlert = function (title, msg) {
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title(title)
                .textContent(msg)
                .ok('OK')
            );
        };


        vm.tryLogin = function () {

            AuthService.login(vm.obj).then(function (isLogged) {
                if (isLogged) {
             

                    $state.go('app.home');
                } else {
                    vm.showAlert("Error", "Something went wrong. Please try again.");
                    vm.obj = {};
                }
            }, function () {
                vm.showAlert("Error", "Couldn't connect to database.");
            });

        }

        

    };

})(SparkleIT.YourAccess);