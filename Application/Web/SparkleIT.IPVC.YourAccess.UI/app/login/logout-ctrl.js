﻿(function(app) {
    'use strict';

    var controllerId = 'LogoutController';
    app.controller(controllerId, LogoutController);

    LogoutController.$inject = ['$state', 'AuthService'];

    function LogoutController($state, AuthService) {

        var vm = this;

        AuthService.logout();
        
        $state.go('app.login');
        
    };

})(SparkleIT.YourAccess);