﻿(function(app) {
    'use strict';

    
    app.factory('AuthService', function ($http, config, $sessionStorage, $rootScope) {
        var authService = {};
 
        authService.login = function (obj) {
            
            return $http
                  .post(config.BuidlUrl('api/user/try-login'), obj)
                  .then(function (res) {

                      if (res.data != null) {
                          $sessionStorage.loggedUser = res.data;
                          $rootScope.$broadcast('$logged');
                      }

                      return res.data;
                  });

        };
 
        authService.isAuthenticated = function () {
            return !!$sessionStorage.loggedUser;
        };

        authService.isAutorized = function (ROLES) {
            return ROLES.indexOf($sessionStorage.loggedUser.user_type) !== -1;
        };
 

        authService.getLoggedUser = function () {
            return $sessionStorage.loggedUser;
        };
       
        authService.logout = function () {
            $sessionStorage.loggedUser = null;
            $rootScope.$broadcast('$logged_out');
        };


        
 
        return authService;
    });


})(SparkleIT.YourAccess);

