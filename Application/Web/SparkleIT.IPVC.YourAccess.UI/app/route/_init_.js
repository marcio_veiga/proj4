﻿(function(app) {
    'use strict';

    app.config(RegisterStates);

    RegisterStates.$inject = ['$stateProvider'];

    function RegisterStates($stateProvider) {

        var moduleStates = getModuleStates();
        moduleStates.forEach(function (s) {            
            $stateProvider.state(s.state, s.config);
        });
    };

    function getModuleStates() {
        return [            
            {
                state: 'app.createRoute',
                config: {
                    url: '/createRoute',
                    templateUrl: 'app/route/routeCreate.html',
                    controller: 'RouteCreateController as vm',
                    data: {
                        requireLogin: true,
                        roles: [0, 1]
                    }
                }
            },
            {
                state: 'app.listRoutes',
                config: {
                    url: '/listRoutes',
                    templateUrl: 'app/route/listRoutes.html',
                    controller: 'listRoutesController as vm',
                    data: {
                        requireLogin: true,
                        roles: [0, 1, 2]
                    }
                }
            }
        ];
    };

})(SparkleIT.YourAccess);

