﻿(function(app) {
    'use strict';

    var controllerId = 'listRoutesController';
    app.controller(controllerId, listRoutesController);

    listRoutesController.$inject = ['$state', '$stateParams', 'RouteService', 'config', '$scope', '$mdToast'];

    function listRoutesController($state, $stateParams, RouteService, config, $scope, $mdToast) {
        
        var vm = this;

        vm.routes = {};

        RouteService.getAllroutes().success(function (data, status) {
            vm.routes = data;
        }).error(function (data, status) {
            alert(error);
        });

        vm.createRoute = function () {
            $state.go('app.createRoute');
        }

        vm.goToProfile = function (id) {
            $state.go('app.profile', { id: id });
        }

        $scope.editRoute = function (route) {
            $scope.current = route;

        };

        $scope.save = function (route) {

            $scope.current = {};
        };

        $scope.apagar = function (route) {

        }

    };

})(SparkleIT.YourAccess);