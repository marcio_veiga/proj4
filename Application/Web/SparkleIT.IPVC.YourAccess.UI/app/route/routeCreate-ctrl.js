﻿(function(app) {
    'use strict';

    var controllerId = 'RouteCreateController';
    app.controller(controllerId, RouteCreateController);

    RouteCreateController.$inject = ['$state', '$stateParams', 'RouteService', 'config', '$scope', '$mdDialog'];

    function RouteCreateController($state, $stateParams, RouteService, config, $scope, $mdDialog) {

        $scope.personal = true;

        var vm = this;

        vm.route = {};


        vm.showAlert = function (title, msg) {
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title(title)
                .textContent(msg)
                .ok('OK')
            );
        };


        $scope.save = function () {

            vm.route.ativo = 1;

            RouteService.saveRoute(vm.route).success(function (data, status) {

                if (data) {
                    vm.showAlert("success", "route successfully created.");
                }
                else {
                    vm.showAlert("error", "error creating route.");
                }
            }).error(function (data, status) {
                vm.showAlert("error", "error creating route.");
            });

        }

    };

})(SparkleIT.YourAccess);