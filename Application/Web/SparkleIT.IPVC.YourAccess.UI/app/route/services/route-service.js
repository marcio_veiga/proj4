﻿(function(app) {
    'use strict';

    
    app.factory('RouteService', function ($http, config, $sessionStorage) {


        var factory = {
            getById: getById,
            getAllroutes: getAllRoutes,
            saveRoute: saveRoute,
        };

        function getById(id) {
            return $http.get(config.BuidlUrl('api/route/getbyid/' + id));
        };


        function getAllRoutes() {
            return $http.get(config.BuidlUrl('api/route/getall'));
        };


        function saveRoute(obj) {
            return $http.post(config.BuidlUrl('api/route/save'), obj);
        };

        return factory;
    });


})(SparkleIT.YourAccess);

