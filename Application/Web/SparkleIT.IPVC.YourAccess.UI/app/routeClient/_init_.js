﻿(function(app) {
    'use strict';

    app.config(RegisterStates);

    RegisterStates.$inject = ['$stateProvider'];

    function RegisterStates($stateProvider) {

        var moduleStates = getModuleStates();
        moduleStates.forEach(function (s) {            
            $stateProvider.state(s.state, s.config);
        });
    };

    function getModuleStates() {
        return [            
            {
                state: 'app.createRouteClient',
                config: {
                    url: '/createRouteClient',
                    templateUrl: 'app/routeClient/routeClientCreate.html',
                    controller: 'RouteClientCreateController as vm',
                    data: {
                        requireLogin: true,
                        roles: [0, 1]
                    }
                }
            },
            {
                state: 'app.listRouteClients',
                config: {
                    url: '/listRouteClients',
                    templateUrl: 'app/routeClient/listRouteClients.html',
                    controller: 'listRouteClientsController as vm',
                    data: {
                        requireLogin: true,
                        roles: [0, 1, 2]
                    }
                }
            }
        ];
    };

})(SparkleIT.YourAccess);

