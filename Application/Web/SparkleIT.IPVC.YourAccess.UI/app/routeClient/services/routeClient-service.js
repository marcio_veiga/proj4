﻿(function(app) {
    'use strict';

    
    app.factory('RouteClientService', function ($http, config, $sessionStorage) {


        var factory = {
            getById: getById,
            getAllrouteClients: getAllRouteClients,
            saveRouteClient: saveRouteClient,
        };

        function getById(id) {
            return $http.get(config.BuidlUrl('api/routeClient/getbyid/' + id));
        };


        function getAllRouteClients() {
            return $http.get(config.BuidlUrl('api/routeClient/getall'));
        };


        function saveRouteClient(obj) {
            return $http.post(config.BuidlUrl('api/routeClient/save'), obj);
        };

        return factory;
    });


})(SparkleIT.YourAccess);

