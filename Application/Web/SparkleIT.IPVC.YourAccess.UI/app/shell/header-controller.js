﻿(function (app) {
    'use strict';

    app.controller('HeaderController', HeaderController);

    HeaderController.$inject = ['$rootScope', 'AuthService', '$scope'];

    function HeaderController($rootScope, AuthService, $scope) {

        var vm = this;
        vm.isAuthenticated = AuthService.isAuthenticated();
        
        $rootScope.$on('$logged', function () {
            vm.isAuthenticated = AuthService.isAuthenticated();

        });

        $rootScope.$on('$logged_out', function () {
            vm.isAuthenticated = null;
        });

    };


})(SparkleIT.YourAccess);