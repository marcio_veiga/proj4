﻿(function (app) {
    'use strict';

    app.controller('NavController', NavController);

    NavController.$inject = ['$rootScope', 'AuthService', '$scope'];

    function NavController($rootScope, AuthService, $scope) {

        var vm = this;
               

        vm.user = AuthService.getLoggedUser();
        
        vm.isAutorized = function (roles) {
            return AuthService.isAutorized(roles);
        }

    };


})(SparkleIT.YourAccess);