﻿(function (app) {
    'use strict';

    app.controller('ShellController', ShellController);

    ShellController.$inject = ['$rootScope','$state'];

    function ShellController($rootScope, $state) {

        var vm = this;
               
        function run() {        
            $state.go('app.login');
        };

        run();

    };


})(SparkleIT.YourAccess);