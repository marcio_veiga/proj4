﻿(function(app) {
    'use strict';

    app.config(RegisterStates);

    RegisterStates.$inject = ['$stateProvider'];

    function RegisterStates($stateProvider) {

        var moduleStates = getModuleStates();
        moduleStates.forEach(function (s) {            
            $stateProvider.state(s.state, s.config);
        });
    };

    function getModuleStates() {
        return [            
            {
                state: 'app.createStorage',
                config: {
                    url: '/createStorage',
                    templateUrl: 'app/storage/storageCreate.html',
                    controller: 'StorageCreateController as vm',
                    data: {
                        requireLogin: true,
                        roles: [0, 1]
                    }
                }
            },
            {
                state: 'app.listStorages',
                config: {
                    url: '/listStorages',
                    templateUrl: 'app/storage/listStorages.html',
                    controller: 'ListStoragesController as vm',
                    data: {
                        requireLogin: true,
                        roles: [0, 1, 2]
                    }
                }
            }
        ];
    };

})(SparkleIT.YourAccess);

