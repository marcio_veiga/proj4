﻿(function(app) {
    'use strict';

    var controllerId = 'ListStoragesController';
    app.controller(controllerId, ListStoragesController);

    ListStoragesController.$inject = ['$state', '$stateParams', 'StorageService', 'config', '$scope', '$mdToast'];

    function ListStoragesController($state, $stateParams, StorageService, config, $scope, $mdToast) {

        $scope.personal = true;

        var vm = this;

        vm.storages = {};

       

        vm.showAlert = function (title, msg) {
            $mdDialog.show(
                $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title(title)
                    .textContent(msg)
                    .ok('OK')
            );
        };

        StorageService.getAllstorages().success(function (data, status) {
            vm.storages = data;
        }).error(function (data, status) {
            alert(error);
        });
        
        $scope.editStorage = function(storage) {
            $scope.current = storage;

        };

        $scope.save = function (storage) {

            $scope.current = {};
        };

        $scope.apagar = function (storage) {

        }
        
    };

})(SparkleIT.YourAccess);