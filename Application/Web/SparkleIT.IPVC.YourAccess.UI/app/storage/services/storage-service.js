﻿(function(app) {
    'use strict';

    
    app.factory('StorageService', function ($http, config, $sessionStorage) {


        var factory = {
            getById: getById,
            getAllstorages: getAllstorages,
            saveStorage: saveStorage,
            getLoggedStorage: getLoggedStorage,
        };

        function getById(id) {
            return $http.get(config.BuidlUrl('api/storage/getbyid/' + id));
        };


        function getAllstorages() {
            return $http.get(config.BuidlUrl('api/storage/getall'));
        };


        function saveStorage(obj) {
            return $http.post(config.BuidlUrl('api/storage/save'), obj);
        };

        function updateStorage(obj) {
            return $http.post(config.BuidlUrl('api/storage/update'), obj);
        };

        function getLoggedStorage () {
            return $sessionStorage.loggedStorage;
        };

        return factory;
        
    });


})(SparkleIT.YourAccess);

