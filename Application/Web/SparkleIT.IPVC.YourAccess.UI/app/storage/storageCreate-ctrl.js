﻿(function(app) {
    'use strict';

    var controllerId = 'StorageCreateController';
    app.controller(controllerId, StorageCreateController);

    StorageCreateController.$inject = ['$state', '$stateParams', 'StorageService', 'config', '$scope', '$mdDialog'];

    function StorageCreateController($state, $stateParams, StorageService, config, $scope, $mdDialog) {

        $scope.personal = true;

        var vm = this;

        vm.loggedStorage = StorageService.getLoggedStorage();

        vm.storage = {};


        vm.showAlert = function (title, msg) {
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title(title)
                .textContent(msg)
                .ok('OK')
            );
        };


        $scope.save = function () {

            vm.storage.ativo = 1;

            StorageService.saveStorage(vm.storage).success(function (data, status) {

                if (data) {
                    vm.showAlert("success", "storage successfully created.");
                }
                else {
                    vm.showAlert("error", "error creating storage.");
                }
            }).error(function (data, status) {
                vm.showAlert("error", "error creating storage.");
            });

        }

       

    };

})(SparkleIT.YourAccess);