﻿(function(app) {
    'use strict';

    app.config(RegisterStates);

    RegisterStates.$inject = ['$stateProvider'];

    function RegisterStates($stateProvider) {

        var moduleStates = getModuleStates();
        moduleStates.forEach(function (s) {            
            $stateProvider.state(s.state, s.config);
        });
    };

    function getModuleStates() {
        return [            
            {
                state: 'app.createTruck',
                config: {
                    url: '/createTruck',
                    templateUrl: 'app/truck/truckCreate.html',
                    controller: 'TruckCreateController as vm',
                    data: {
                        requireLogin: true,
                        roles: [0, 1]
                    }
                }
            },
            {
                state: 'app.listTrucks',
                config: {
                    url: '/listTrucks',
                    templateUrl: 'app/truck/listTrucks.html',
                    controller: 'listTrucksController as vm',
                    data: {
                        requireLogin: true,
                        roles: [0, 1, 2]
                    }
                }
            }
        ];
    };

})(SparkleIT.YourAccess);

