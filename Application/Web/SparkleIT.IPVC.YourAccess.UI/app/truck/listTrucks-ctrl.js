﻿(function(app) {
    'use strict';

    var controllerId = 'listTrucksController';
    app.controller(controllerId, listTrucksController);

    listTrucksController.$inject = ['$state', '$stateParams', 'TruckService', 'config', '$scope', '$mdToast','$modal'];

    function listTrucksController($state, $stateParams, TruckService, config, $scope, $mdToast, $modal) {
        
        var vm = this;

        vm.trucks = {};

        vm.open = function (truckTest) {
            $modal.open({
                templateUrl: 'myModalContent.html', // loads the template
                backdrop: true, // setting backdrop allows us to close the modal window on clicking outside the modal window
                windowClass: 'modal', // windowClass - additional CSS class(es) to be added to a modal window template
                controller: function ($scope, $modalInstance, $log, truck, TruckService) {


                    $scope.current = truckTest;
                    vm.truck = $scope.current;

                    $scope.submit = function () {
                        $log.log('Submiting truck info.'); // kinda console logs this statement

                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                },
                resolve: {
                    truck: function () {
                        return $scope.truck;
                    }
                }
            });//end of modal.open
        }; // end of scope.open function

        TruckService.getAlltrucks().success(function (data, status) {
            vm.trucks = data;
        }).error(function (data, status) {
            alert(error);
        });

        vm.createTruck = function () {
            $state.go('app.createTruck');
        }

        vm.goToProfile = function (id) {
            $state.go('app.profile', { id: id });
        }


        $scope.submit = function () {

            TruckService.updateTruck(vm.truck).success(function (data, status) {

                if (data) {
                    vm.showAlert("success", "truck successfully created.");
                }
                else {
                    vm.showAlert("error", "error creating truck.");
                }
            }).error(function (data, status) {
                vm.showAlert("error", "error creating truck.");
            });

        };

        $scope.apagar = function (truck) {

        }

    };

})(SparkleIT.YourAccess);