﻿(function(app) {
    'use strict';

    
    app.factory('TruckService', function ($http, config, $sessionStorage) {


        var factory = {
            getById: getById,
            getAlltrucks: getAllTrucks,
            saveTruck: saveTruck,
        };

        function getById(id) {
            return $http.get(config.BuidlUrl('api/truck/getbyid/' + id));
        };


        function getAllTrucks() {
            return $http.get(config.BuidlUrl('api/truck/getall'));
        };


        function saveTruck(obj) {
            return $http.post(config.BuidlUrl('api/truck/save'), obj);
        };

        return factory;
    });


})(SparkleIT.YourAccess);

