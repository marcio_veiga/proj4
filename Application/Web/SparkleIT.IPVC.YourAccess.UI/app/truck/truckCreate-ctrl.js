﻿(function(app) {
    'use strict';

    var controllerId = 'TruckCreateController';
    app.controller(controllerId, TruckCreateController);

    TruckCreateController.$inject = ['$state', '$stateParams', 'TruckService', 'config', '$scope', '$mdDialog'];

    function TruckCreateController($state, $stateParams, TruckService, config, $scope, $mdDialog) {

        $scope.personal = true;

        var vm = this;

        vm.truck = {};


        vm.showAlert = function (title, msg) {
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title(title)
                .textContent(msg)
                .ok('OK')
            );
        };


        $scope.save = function () {
            TruckService.saveTruck(vm.truck).success(function (data, status) {

                if (data) {
                    vm.showAlert("success", "truck successfully created.");
                }
                else {
                    vm.showAlert("error", "error creating truck.");
                }
            }).error(function (data, status) {
                vm.showAlert("error", "error creating truck.");
            });

        }

    };

})(SparkleIT.YourAccess);