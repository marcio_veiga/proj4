﻿(function(app) {
    'use strict';

    app.config(RegisterStates);

    RegisterStates.$inject = ['$stateProvider'];

    function RegisterStates($stateProvider) {

        var moduleStates = getModuleStates();
        moduleStates.forEach(function (s) {            
            $stateProvider.state(s.state, s.config);
        });
    };

    function getModuleStates() {
        return [            
            {
                state: 'app.createUser',
                config: {
                    url: '/createUser',
                    templateUrl: 'app/user/userCreate.html',
                    controller: 'UserCreateController as vm',
                    data: {
                        requireLogin: true,
                        roles: [0, 1]
                    }
                }
            },
            {
                state: 'app.listUsers',
                config: {
                    url: '/listUsers',
                    templateUrl: 'app/user/listUsers.html',
                    controller: 'ListUsersController as vm',
                    data: {
                        requireLogin: true,
                        roles: [0, 1, 2]
                    }
                }
            },
            {
                state: 'app.profile',
                config: {
                    url: '/profile?id',
                    templateUrl: 'app/user/profile.html',
                    controller: 'ProfileController as vm',
                    data: {
                        requireLogin: true,
                        roles: [0, 1, 2]
                    }
                }
            }
        ];
    };

})(SparkleIT.YourAccess);

