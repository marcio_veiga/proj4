﻿(function (app) {
    'use strict';

    var controllerId = 'ListUsersController';
    app.controller(controllerId, ListUsersController);

    ListUsersController.$inject = ['$state', '$stateParams', 'UserService', 'config', '$scope', '$mdToast', '$modal'];

    function ListUsersController($state, $stateParams, UserService, config, $scope, $mdToast, $modal) {

        $scope.personal = true;

        var vm = this;

        vm.users = {};

        vm.open = function (userTeste) {
            $modal.open({
                templateUrl: 'myModalContent.html', // loads the template
                backdrop: true, // setting backdrop allows us to close the modal window on clicking outside the modal window
                windowClass: 'modal', // windowClass - additional CSS class(es) to be added to a modal window template
                controller: function ($scope, $modalInstance, $log, user, UserService) {


                    $scope.current = userTeste;
                    vm.user = $scope.current;

                    $scope.submit = function () {
                        $log.log('Submiting user info.'); // kinda console logs this statement

                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                },
                resolve: {
                    user: function () {
                        return $scope.user;
                    }
                }
            });//end of modal.open
        }; // end of scope.open function

        vm.showAlert = function (title, msg) {
            $mdDialog.show(
                $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title(title)
                    .textContent(msg)
                    .ok('OK')
            );
        };

        UserService.getAllusers().success(function (data, status) {
            vm.users = data;
        }).error(function (data, status) {
            alert(error);
        });


        $scope.submit = function () {

            UserService.updateUser(vm.user).success(function (data, status) {

                if (data) {
                    vm.showAlert("success", "user successfully created.");
                }
                else {
                    vm.showAlert("error", "error creating user.");
                }
            }).error(function (data, status) {
                vm.showAlert("error", "error creating user.");
            });

        };

        $scope.apagar = function (user) {

        };

        $scope.dtColums

    };

})(SparkleIT.YourAccess);