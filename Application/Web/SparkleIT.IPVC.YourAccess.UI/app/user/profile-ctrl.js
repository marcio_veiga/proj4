﻿(function(app) {
    'use strict';

    var controllerId = 'ProfileController';
    app.controller(controllerId, ProfileController);

    ProfileController.$inject = ['$state', '$stateParams', 'UserService', 'config', '$scope', '$mdToast','$timeout'];

    function ProfileController($state, $stateParams, UserService, config, $scope, $mdToast, $timeout) {

        var vm = this;

        vm.config = config;

        

        function activate() {

            UserService.getById($stateParams.id).success(function (data, status) {
                
                vm.user = data;
                if (data.date_of_birth.indexOf("0001") !== -1) {
                    vm.user.date_of_birth = "";
                }
                
            }).error(function (data, status) {
                alert("Error getting user info");
            });

            
        }

        $timeout(function () {

            var areaChartData = {
                labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                datasets: [
                  {
                      label: "Electronics",
                      fillColor: "rgba(210, 214, 222, 1)",
                      strokeColor: "rgba(210, 214, 222, 1)",
                      pointColor: "rgba(210, 214, 222, 1)",
                      pointStrokeColor: "#c1c7d1",
                      pointHighlightFill: "#fff",
                      pointHighlightStroke: "rgba(220,220,220,1)",
                      data: [20, 21, 19, 21, 20, 13, 23,3]
                  }
                ]
            };
            //-------------
            //- BAR CHART -
            //-------------
            var barChartCanvas = $("#barChart").get(0).getContext("2d");
            var barChart = new Chart(barChartCanvas);
            var barChartData = areaChartData;
            barChartData.datasets[0].fillColor = "#00a65a";
            barChartData.datasets[0].strokeColor = "#00a65a";
            barChartData.datasets[0].pointColor = "#00a65a";
            var barChartOptions = {
                //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                scaleBeginAtZero: true,
                //Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines: true,
                //String - Colour of the grid lines
                scaleGridLineColor: "rgba(0,0,0,.05)",
                //Number - Width of the grid lines
                scaleGridLineWidth: 1,
                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,
                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines: true,
                //Boolean - If there is a stroke on each bar
                barShowStroke: true,
                //Number - Pixel width of the bar stroke
                barStrokeWidth: 2,
                //Number - Spacing between each of the X value sets
                barValueSpacing: 5,
                //Number - Spacing between data sets within X values
                barDatasetSpacing: 1,
                //String - A legend template
                legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
                //Boolean - whether to make the chart responsive
                responsive: true,
                maintainAspectRatio: true
            };

            barChartOptions.datasetFill = false;
            barChart.Bar(barChartData, barChartOptions);

        }, 200);

        

        if ($stateParams.id) {

            activate();

        }



              
    };


})(SparkleIT.YourAccess);

