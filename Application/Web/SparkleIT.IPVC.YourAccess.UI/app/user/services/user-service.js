﻿(function(app) {
    'use strict';

    
    app.factory('UserService', function ($http, config, $sessionStorage) {


        var factory = {
            getById: getById,
            getAllusers: getAllusers,
            saveUser: saveUser,
            getLoggedUser: getLoggedUser,
        };



        function getById(id) {
            return $http.get(config.BuidlUrl('api/user/getbyid/' + id));
        };


        function getAllusers() {
            return $http.get(config.BuidlUrl('api/user/getall'));
        };


        function saveUser(obj) {
            return $http.post(config.BuidlUrl('api/user/save'), obj);
        };


        function getLoggedUser () {
            return $sessionStorage.loggedUser;
        };

        return factory;
    });


})(SparkleIT.YourAccess);

