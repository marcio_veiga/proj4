﻿(function(app) {
    'use strict';

    var controllerId = 'UserCreateController';
    app.controller(controllerId, UserCreateController);

    UserCreateController.$inject = ['$state', '$stateParams', 'UserService', 'config', '$scope', '$mdDialog'];

    function UserCreateController($state, $stateParams, UserService, config, $scope, $mdDialog) {

        $scope.personal = true;

        var vm = this;

        vm.loggedUser = UserService.getLoggedUser();

        vm.user = {};


        vm.showAlert = function (title, msg) {
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title(title)
                .textContent(msg)
                .ok('OK')
            );
        };


        $scope.save = function () {
            UserService.saveUser(vm.user).success(function (data, status) {

                if (data) {
                    vm.showAlert("success", "user successfully created.");
                }
                else {
                    vm.showAlert("error", "error creating user.");
                }
            }).error(function (data, status) {
                vm.showAlert("error", "error creating user.");
            });

        }


        $scope.userTypes = [{ 'level': '0', 'name': 'System Admin' },
                            { 'level': '1', 'name': 'Zone Admin' },
                            { 'level': '2', 'name': 'Simple User' }];

    };

})(SparkleIT.YourAccess);