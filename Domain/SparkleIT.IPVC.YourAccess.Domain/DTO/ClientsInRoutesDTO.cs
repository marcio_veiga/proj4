﻿using NPoco;
using System;

namespace SparkleIT.IPVC.YourAccess.Domain.Model
{
    public class ClientsInRoutesDTO
    {

        public long idCliente { get; set; }
        public decimal latitude { get; set; }
        public decimal longitude { get; set; }
        public long fkRota { get; set; }
        public string descricao { get; set; }
        public decimal armazemLat { get; set; }
        public decimal armazemLon { get; set; }

        public ClientsInRoutesDTO()
        {

        }

        public ClientsInRoutesDTO(long idCliente, decimal latitude, decimal longitude, long fkRota, string descricao, decimal armazemLat, decimal armazemLon)
        {
            this.idCliente = idCliente;
            this.latitude = latitude;
            this.longitude = longitude;
            this.fkRota = fkRota;
            this.descricao = descricao;
            this.armazemLat = armazemLat;
            this.armazemLon = armazemLon;

        }

    }
}
