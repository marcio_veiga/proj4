﻿using NPoco;
using System;

namespace SparkleIT.IPVC.YourAccess.Domain.Model
{
    public class RouteClientDTO
    {

        public long fkRota { get; set; }
        public long fkCliente { get; set; }

    }
}
