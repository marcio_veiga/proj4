﻿using NPoco;
using System;

namespace SparkleIT.IPVC.YourAccess.Domain.Model
{
    public class RouteDTO
    {

        public long idRota { get; set; }
        public string descricao { get; set; }
        public long fkArmazem { get; set; }
        public long fkCamiao { get; set; }
        public int ativo { get; set; }
        public int estado { get; set; }

    }
}
