﻿using NPoco;
using System;

namespace SparkleIT.IPVC.YourAccess.Domain.Model
{
    public class StorageDTO
    {

        public long idArmazem { get; set; }
        public string nome { get; set; }
        public string morada { get; set; }
        public decimal latitude { get; set; }
        public decimal longitude { get; set; }
        public int ativo { get; set; }
    }
}