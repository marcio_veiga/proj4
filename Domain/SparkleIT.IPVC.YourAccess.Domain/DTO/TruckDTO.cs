﻿using NPoco;
using System;

namespace SparkleIT.IPVC.YourAccess.Domain.Model
{
    public class TruckDTO
    {
       
        public long idCamiao { get; set; }
        public string nome { get; set; }
        public int estado { get; set; }
        public int ativo { get; set; }
    }
}
