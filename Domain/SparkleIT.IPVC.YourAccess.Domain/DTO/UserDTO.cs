﻿using NPoco;
using System;

namespace SparkleIT.IPVC.YourAccess.Domain.Model
{
    public class UserDTO
    {
       
        public long id { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public int user_type { get; set; }
        public int ativo { get; set; }

    }
}
