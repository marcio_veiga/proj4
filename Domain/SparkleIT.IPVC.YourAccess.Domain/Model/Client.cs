﻿using NPoco;
using System;

namespace SparkleIT.IPVC.YourAccess.Domain.Model
{
    [TableName("cliente")]
    [PrimaryKey("idCliente", AutoIncrement = true)]
    public class Client
    {
        [Column("idCliente")]
        public int idCliente { get; set; }

        [Column("nome")]
        public string nome { get; set; }

        [Column("morada")]
        public string morada { get; set; }

        [Column("latitude")]
        public decimal latitude { get; set; }

        [Column("longitude")]
        public decimal longitude { get; set; }

        [Column("ativo")]
        public int ativo { get; set; }

        [Column("estado")]
        public int estado { get; set; }

    }
}
