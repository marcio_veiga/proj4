﻿using NPoco;
using System;

namespace SparkleIT.IPVC.YourAccess.Domain.Model
{
    [TableName("rota")]
    [PrimaryKey("idRota", AutoIncrement = true)]
    public class Route
    {
        [Column("idRota")]
        public int idRota { get; set; }

        [Column("descricao")]
        public string descricao { get; set; }

        [Column("fkArmazem")]
        public long fkArmazem { get; set; }

        [Column("fkCamiao")]
        public long fkCamiao { get; set; }

        [Column("ativo")]
        public int ativo { get; set; }

        [Column("estado")]
        public int estado { get; set; }

    }
}
