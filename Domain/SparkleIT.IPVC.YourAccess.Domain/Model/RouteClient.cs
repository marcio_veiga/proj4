﻿using NPoco;
using System;

namespace SparkleIT.IPVC.YourAccess.Domain.Model
{
    [TableName("rota_cliente")]
    [PrimaryKey("id", AutoIncrement = true)]
    public class RouteClient
    {
        [Column("id")]
        public int idRota { get; set; }

        [Column("fkRota")]
        public long fkRota { get; set; }

        [Column("fkCliente")]
        public long fkCliente { get; set; }

    }
}
