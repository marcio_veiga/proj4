﻿using NPoco;
using System;

namespace SparkleIT.IPVC.YourAccess.Domain.Model
{
    [TableName("armazem")]
    [PrimaryKey("idArmazem", AutoIncrement = true)]
    public class Storage
    {
        [Column("idArmazem")]
        public int idArmazem { get; set; }

        [Column("nome")]
        public string nome { get; set; }

        [Column("morada")]
        public string morada { get; set; }

        [Column("latitude")]
        public decimal latitude { get; set; }

        [Column("longitude")]
        public decimal longitude { get; set; }

        [Column("ativo")]
        public int ativo { get; set; }
    }
}