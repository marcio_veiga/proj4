﻿using NPoco;
using System;

namespace SparkleIT.IPVC.YourAccess.Domain.Model
{
    [TableName("camiao")]
    [PrimaryKey("idCamiao", AutoIncrement = true)]
    public class Truck
    {
        [Column("idCamiao")]
        public int idCamiao { get; set; }

        [Column("nome")]
        public string nome { get; set; }

        [Column("estado")]
        public int estado { get; set; }

        [Column("ativo")]
        public int ativo { get; set; }
    }
}
