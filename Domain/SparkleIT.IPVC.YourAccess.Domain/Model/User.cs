﻿using NPoco;
using System;

namespace SparkleIT.IPVC.YourAccess.Domain.Model
{
    [TableName("user")]
    [PrimaryKey("id", AutoIncrement = true)]
    public class User
    {
        [Column("id")]
        public int idUtilizador { get; set; }

        [Column("username")]
        public string username { get; set; }

        [Column("password")]
        public string password { get; set; }

        [Column("name")]
        public string name { get; set; }

        [Column("user_type")]
        public int user_type { get; set; }

        [Column("ativo")]
        public int ativo { get; set; }
    }
}
