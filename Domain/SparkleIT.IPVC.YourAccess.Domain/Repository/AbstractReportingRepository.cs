﻿using NPoco;

namespace SparkleIT.IPVC.YourAccess.Domain
{
    public abstract class AbstractReportingRepository
    {
        private readonly string _connectionStringName;

        protected AbstractReportingRepository(){
            _connectionStringName = Constants.Constants.connectionStringName;
        }
        
        protected virtual Database GetDatabase()
        {
            return new Database(_connectionStringName);
        }
    }
}