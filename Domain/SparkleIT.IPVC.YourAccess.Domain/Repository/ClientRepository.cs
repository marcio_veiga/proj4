﻿using System;
using System.Collections.Generic;
using NPoco;
using SparkleIT.IPVC.YourAccess.Domain.Model;

namespace SparkleIT.IPVC.YourAccess.Domain.Repository
{
    public interface IClientRepository
    {
        List<Client> GetAll();
        ClientDTO GetById(int id);
        void Update(Client client);
        bool Save(Client client);

        List<ClientsInRoutesDTO> GetClientsInRoutes();
    }

    public class ClientRepository : AbstractReportingRepository, IClientRepository
    {
        public ClientRepository() { }

        public List<Client> GetAll()
        {
            using (Database db = GetDatabase())
            {
                return db.Fetch<Client>();
            }
        }

        public ClientDTO GetById(int id)
        {
            var querie = Sql.Builder.Select(@" 
                            
                            [client].*
                        ")
                        .From("[client]")
                        .Where("[client].id = @0", id);


            using (Database db = GetDatabase())
            {
                return db.SingleOrDefault<ClientDTO>(querie);
            }
        }

        public List<ClientsInRoutesDTO> GetClientsInRoutes()
        {


            var querie = Sql.Builder.Select(@" 
             t1.idCliente, t1.latitude, t1.longitude, t2.fkRota, t3.descricao, t4.latitude as armazemLat, t4.longitude as armazemLon
                        ")
                       .From("cliente t1")
                       .InnerJoin("rota_cliente t2").On("t2.fkCliente = t1.idCliente")
                       .InnerJoin("rota t3").On("t3.idRota = t2.fkRota")
                       .InnerJoin("armazem t4").On("t4.idArmazem = t3.fkArmazem");


            System.Diagnostics.Debug.WriteLine("TESTE" + querie.SQL.ToString());


            try
            {

                using (Database db = GetDatabase())
            {
                return db.Fetch<ClientsInRoutesDTO>(querie);
            }

            }
            catch (Exception ex)
            {

                System.Diagnostics.Debug.WriteLine(ex);

                return null;

            }
        }

        public bool Save(Client client)
        {
            System.Diagnostics.Debug.WriteLine(client.latitude);
            try
            {
                using (Database db = GetDatabase())
                {
                    db.Save(client);
                }
                return true;
            }
            catch(Exception ex) {

                System.Diagnostics.Debug.WriteLine(ex);

                return false;

            }
        }


        public void Update(Client client)
        {
            using (Database db = GetDatabase())
            {
                db.Update(client);
            }
        }

    }
}
