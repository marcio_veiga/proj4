﻿using System;
using System.Collections.Generic;
using NPoco;
using SparkleIT.IPVC.YourAccess.Domain.Model;

namespace SparkleIT.IPVC.YourAccess.Domain.Repository
{
    public interface IRouteClientRepository
    {
        List<RouteClient> GetAll();
        RouteClientDTO GetById(int id);
        void Update(RouteClient routeClient);
        bool Save(RouteClient routeClient);
    }

    public class RouteClientRepository : AbstractReportingRepository, IRouteClientRepository
    {
        public RouteClientRepository() { }

        public List<RouteClient> GetAll()
        {
            using (Database db = GetDatabase())
            {
                return db.Fetch<RouteClient>();
            }
        }

        public RouteClientDTO GetById(int id)
        {
            var querie = Sql.Builder.Select(@" 
                            
                            [routeClient].*
                        ")
                        .From("[routeClient]")
                        .Where("[routeClient].id = @0", id);


            using (Database db = GetDatabase())
            {
                return db.SingleOrDefault<RouteClientDTO>(querie);
            }
        }
      
        public bool Save(RouteClient routeClient)
        {
            System.Diagnostics.Debug.WriteLine(routeClient.idRota);
            try
            {
                using (Database db = GetDatabase())
                {
                    db.Save(routeClient);
                }
                return true;
            }
            catch(Exception ex) {

                System.Diagnostics.Debug.WriteLine(ex);

                return false;

            }
        }


        public void Update(RouteClient routeClient)
        {
            using (Database db = GetDatabase())
            {
                db.Update(routeClient);
            }
        }

    }
}
