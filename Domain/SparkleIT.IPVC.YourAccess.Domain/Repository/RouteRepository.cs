﻿using System;
using System.Collections.Generic;
using NPoco;
using SparkleIT.IPVC.YourAccess.Domain.Model;

namespace SparkleIT.IPVC.YourAccess.Domain.Repository
{
    public interface IRouteRepository
    {
        List<Route> GetAll();
        RouteDTO GetById(int id);
        void Update(Route route);
        int Save(Route route);
    }

    public class RouteRepository : AbstractReportingRepository, IRouteRepository
    {
        public RouteRepository() { }

        public List<Route> GetAll()
        {
            using (Database db = GetDatabase())
            {
                return db.Fetch<Route>();
            }
        }

        public RouteDTO GetById(int id)
        {
            var querie = Sql.Builder.Select(@" 
                            
                            [route].*
                        ")
                        .From("[route]")
                        .Where("[route].id = @0", id);


            using (Database db = GetDatabase())
            {
                return db.SingleOrDefault<RouteDTO>(querie);
            }
        }
      
        public int Save(Route route)
        {
            System.Diagnostics.Debug.WriteLine(route.idRota);
            try
            {
                using (Database db = GetDatabase())
                {
                    db.Save(route);
                }
                return route.idRota;
            }
            catch(Exception ex) {

                System.Diagnostics.Debug.WriteLine(ex);

                return -1;

            }
        }


        public void Update(Route route)
        {
            using (Database db = GetDatabase())
            {
                db.Update(route);
            }
        }

    }
}
