﻿using System;
using System.Collections.Generic;
using NPoco;
using SparkleIT.IPVC.YourAccess.Domain.Model;

namespace SparkleIT.IPVC.YourAccess.Domain.Repository
{
    public interface IStorageRepository
    {
        List<Storage> GetAll();
        StorageDTO GetById(int id);
        void Update(Storage storage);
        bool Save(Storage storage);
    }

    public class StorageRepository : AbstractReportingRepository, IStorageRepository
    {
        public StorageRepository() { }

        public List<Storage> GetAll()
        {
            using (Database db = GetDatabase())
            {
                return db.Fetch<Storage>();
            }
        }

        public StorageDTO GetById(int id)
        {
            var querie = Sql.Builder.Select(@" 
                            
                            [storage].*
                        ")
                        .From("[storage]")
                        .Where("[storage].id = @0", id);


            using (Database db = GetDatabase())
            {
                return db.SingleOrDefault<StorageDTO>(querie);
            }
        }
     
        

        public bool Save(Storage storage)
        {
            System.Diagnostics.Debug.WriteLine(storage.idArmazem);
            try
            {
                using (Database db = GetDatabase())
                {
                    db.Save(storage);
                }
                return true;
            }
            catch(Exception ex) {

                System.Diagnostics.Debug.WriteLine(ex);

                return false;

            }
        }


        public void Update(Storage storage)
        {
            using (Database db = GetDatabase())
            {
                db.Update(storage);
            }
        }

    }
}
