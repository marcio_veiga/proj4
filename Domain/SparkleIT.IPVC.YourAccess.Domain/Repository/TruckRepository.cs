﻿using System;
using System.Collections.Generic;
using NPoco;
using SparkleIT.IPVC.YourAccess.Domain.Model;

namespace SparkleIT.IPVC.YourAccess.Domain.Repository
{
    public interface ITruckRepository
    {
        List<Truck> GetAll();
        TruckDTO GetById(int id);
        void Update(Truck truck);
        bool Save(Truck truck);
    }

    public class TruckRepository : AbstractReportingRepository, ITruckRepository
    {
        public TruckRepository() { }

        public List<Truck> GetAll()
        {
            using (Database db = GetDatabase())
            {
                return db.Fetch<Truck>();
            }
        }

        public TruckDTO GetById(int id)
        {
            var querie = Sql.Builder.Select(@" 
                            
                            [camiao].*
                        ")
                        .From("[camiao]")
                        .Where("[camiao].id = @0", id);


            using (Database db = GetDatabase())
            {
                return db.SingleOrDefault<TruckDTO>(querie);
            }
        }
        

        public bool Save(Truck truck)
        {
            System.Diagnostics.Debug.WriteLine(truck.idCamiao);
            try
            {
                using (Database db = GetDatabase())
                {
                    db.Save(truck);
                }
                return true;
            }
            catch(Exception ex) {

                System.Diagnostics.Debug.WriteLine(ex);

                return false;

            }
        }


        public void Update(Truck truck)
        {
            using (Database db = GetDatabase())
            {
                db.Update(truck);
            }
        }

    }
}
