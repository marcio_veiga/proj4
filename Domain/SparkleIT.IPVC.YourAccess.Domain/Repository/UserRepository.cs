﻿using System;
using System.Collections.Generic;
using NPoco;
using SparkleIT.IPVC.YourAccess.Domain.Model;

namespace SparkleIT.IPVC.YourAccess.Domain.Repository
{
    public interface IUserRepository
    {
        List<User> GetAll();
        UserDTO GetById(int id);
        void Update(User user);
        UserDTO GetAuth(string username, string password);
        bool Save(User user);
    }

    public class UserRepository : AbstractReportingRepository, IUserRepository
    {
        public UserRepository() { }

        public List<User> GetAll()
        {
            using (Database db = GetDatabase())
            {
                return db.Fetch<User>();
            }
        }

        public UserDTO GetById(int id)
        {
            var querie = Sql.Builder.Select(@" 
                            
                            [user].*
                        ")
                        .From("[user]")
                        .Where("[user].id = @0", id);


            using (Database db = GetDatabase())
            {
                return db.SingleOrDefault<UserDTO>(querie);
            }
        }
      

        public UserDTO GetAuth(string username,string password)
        {
            try
            {
                using (Database db = GetDatabase())
                  
                {
                  

                    var querie = Sql.Builder.Select(@" 
                            [user].*
                        ")
                                .From("[user]")
                                .Where("[user].username=@0 AND [user].password=@1", username, password);

                    UserDTO user = db.SingleOrDefault<UserDTO>(querie);
                    System.Diagnostics.Debug.WriteLine(user.name.ToString());
                    return user;
                }
            }
            catch (Exception ex) {

                return null; }
        }
        

        public bool Save(User user)
        {
            System.Diagnostics.Debug.WriteLine(user.idUtilizador);
            try
            {
                using (Database db = GetDatabase())
                {
                    db.Save(user);
                }
                return true;
            }
            catch(Exception ex) {

                System.Diagnostics.Debug.WriteLine(ex);

                return false;

            }
        }


        public void Update(User user)
        {
            using (Database db = GetDatabase())
            {
                db.Update(user);
            }
        }

    }
}
