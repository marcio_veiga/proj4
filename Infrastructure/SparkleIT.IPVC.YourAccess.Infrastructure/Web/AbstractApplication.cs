using System;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace SparkleIT.IPVC.YourAccess.Infrastructure.Web
{
    public class AbstractApplication : HttpApplication
    {
        #region App wide events

        public virtual void Application_Start(object sender, EventArgs e)
        {
            OnApplicationStart();
        }

        public virtual void Application_End(object sender, EventArgs e)
        {
            OnEndApplication();
        }

        public void Application_BeginRequest(Object sender, EventArgs e)
        {
            OnBeginRequest(sender as HttpApplication);
        }

        public void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            OnAuthenticateRequest(sender as HttpApplication);
        }

        public void Application_AuthorizeRequest(Object sender, EventArgs e)
        {
            OnAuthorizeRequest(sender as HttpApplication);
        }

        public void Application_ResolveRequestCache(Object sender, EventArgs e)
        {
            OnResolveRequestCache(sender as HttpApplication);
        }

        public void Application_AcquireRequestState(Object sender, EventArgs e)
        {
            OnAcquireRequestState(sender as HttpApplication);
        }

        public void Application_PreRequestHandlerExecute(Object sender, EventArgs e)
        {
            OnPreRequestHandlerExecute(sender as HttpApplication);
        }

        public void Application_PreSendRequestHeaders(Object sender, EventArgs e)
        {
            OnPreSendRequestHeaders(sender as HttpApplication);
        }

        public void Application_PreSendRequestContent(Object sender, EventArgs e)
        {
            OnPreSendRequestContent(sender as HttpApplication);
        }

        public void Application_PostRequestHandlerExecute(Object sender, EventArgs e)
        {
            OnPostRequestHandlerExecute(sender as HttpApplication);
        }

        public void Application_ReleaseRequestState(Object sender, EventArgs e)
        {
            OnReleaseRequestState(sender as HttpApplication);
        }

        public void Application_UpdateRequestCache(Object sender, EventArgs e)
        {
            OnUpdateRequestCache(sender as HttpApplication);
        }

        public void Application_EndRequest(Object sender, EventArgs e)
        {
            OnEndRequest(sender as HttpApplication);
        }

        public void Application_Error(Object sender, EventArgs e)
        {
            OnError(sender as HttpApplication);
        }

        #endregion

        #region Redefined Events

        protected virtual void OnApplicationStart()
        {
        }

        protected virtual void OnEndApplication()
        {
        }

        protected virtual void OnBeginRequest(HttpApplication application)
        {
        }

        protected virtual void OnAuthenticateRequest(HttpApplication application)
        {
        }

        protected virtual void OnEndRequest(HttpApplication application)
        {
        }

        protected virtual void OnError(HttpApplication application)
        {
        }

        protected virtual void OnAuthorizeRequest(HttpApplication application)
        {
        }

        protected virtual void OnResolveRequestCache(HttpApplication application)
        {
        }

        protected virtual void OnAcquireRequestState(HttpApplication application)
        {
        }

        protected virtual void OnPreRequestHandlerExecute(HttpApplication application)
        {
        }

        protected virtual void OnPreSendRequestHeaders(HttpApplication application)
        {
        }

        protected virtual void OnPreSendRequestContent(HttpApplication application)
        {
        }

        protected virtual void OnPostRequestHandlerExecute(HttpApplication application)
        {
        }

        protected virtual void OnReleaseRequestState(HttpApplication application)
        {
        }

        protected virtual void OnUpdateRequestCache(HttpApplication application)
        {
        }

        #endregion

        #region static requests

        private static readonly List<string> FileExtensionsToIgnore = new List<string>
        {
            ".css",
            ".js",
            ".jpg",
            ".gif",
            ".png",
            ".html",
            ".htm",
            ".pdf",
            ".ico",
        };

        protected virtual bool IsStaticRequest(HttpRequest request)
        {
            bool isStaticFile = false;

            if (request.Url.Segments.Length >= 2) // ignorar /content e /scripts
            {
                string resourcesPath = request.Url.Segments[1];
                if (
                    resourcesPath.Equals("app/", StringComparison.InvariantCultureIgnoreCase)
                    || resourcesPath.Equals("content/", StringComparison.InvariantCultureIgnoreCase)
                    || resourcesPath.Equals("scripts/", StringComparison.InvariantCultureIgnoreCase))
                {
                    isStaticFile = true;
                }
            }
            else
            {
                try // ver se � uma extens�o est�tica....png, .js, .css, etc...
                {
                    var file = new FileInfo(request.Url.Segments[request.Url.Segments.Length - 1]);

                    string extension = file.Extension.ToLowerInvariant();

                    isStaticFile = FileExtensionsToIgnore.Contains(extension);
                }
                catch
                {
                    // log ?!... n�...
                }
            }

            return isStaticFile;
        }

        protected virtual bool IsWebApiRequest(HttpRequest request)
        {
            bool isWebApiRequest = false;

            if (request.Url.Segments.Length >= 2)
            {
                string resourcesPath = request.Url.Segments[1];
                if (resourcesPath.Equals("api/", StringComparison.InvariantCultureIgnoreCase))
                {
                    isWebApiRequest = true;
                }
            }

            return isWebApiRequest;
        }


        protected virtual bool RequiresAuthentication(HttpRequest request)
        {
            return !IsStaticRequest(request);
        }

        #endregion
    }
}