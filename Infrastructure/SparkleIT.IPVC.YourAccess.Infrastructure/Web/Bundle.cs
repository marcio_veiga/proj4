﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Optimization;
using BundleTransformer.Core.Builders;
using BundleTransformer.Core.Orderers;
using BundleTransformer.Core.Transformers;
using BundleTransformer.JsMin.Minifiers;
using BundleTransformer.Yui.Minifiers;

namespace SparkleIT.IPVC.YourAccess.Infrastructure.Web
{
    // a versão do minify da microsoft não funciona com algumas librarias de js

    public abstract class AbstractBundle : Bundle
    {
        protected AbstractBundle(string virtualPath, params IBundleTransform[] transforms)
            : base(virtualPath, transforms)
        {
            Files = new List<string>();
        }

        public virtual void AddFiles(params string[] files)
        {
            foreach (var file in files)
            {
                Files.Add(file);
            }
            Include(files);
        }

        public IList<string> Files { get; private set; }
    }

    public class JavascriptBundle : AbstractBundle
    {
        public JavascriptBundle(string virtualPath)
            : base(virtualPath, new ScriptTransformer(new CrockfordJsMinifier()))
        {
            Builder = new NullBuilder();
            Orderer = new NullOrderer();
            ConcatenationToken = ";" + Environment.NewLine;
        }
    }

    public class CssBundle : AbstractBundle
    {
        public CssBundle(string virtualPath)
            : base(virtualPath, new StyleTransformer(new YuiCssMinifier()))
        {
            Builder = new NullBuilder();
            Orderer = new NullOrderer();
        }
    }

    public class CompositeJavascriptBundle : Bundle
    {
        public CompositeJavascriptBundle(string virtualPath)
            : base(virtualPath, new ScriptTransformer(new CrockfordJsMinifier()))
        {
            Builder = new NullBuilder();
            Orderer = new NullOrderer();
            ConcatenationToken = ";" + Environment.NewLine;
        }

        public virtual void AddFiles(params JavascriptBundle[] bundles)
        {
            foreach (var bundle in bundles)
            {
                Include(bundle.Files.ToArray());
            }
        }
    }

    public class CompositeCssBundle : Bundle
    {
        public CompositeCssBundle(string virtualPath)
            : base(virtualPath, new StyleTransformer(new YuiCssMinifier()))
        {
            Builder = new NullBuilder();
            Orderer = new NullOrderer();
        }

        public virtual void AddFiles(params CssBundle[] bundles)
        {
            foreach (var bundle in bundles)
            {
                Include(bundle.Files.ToArray());
            }
        }
    }
}