﻿using System;
using System.IO;
using System.Web.Http;
using System.Web.Mvc;
using Castle.Facilities.Logging;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Microsoft.Practices.ServiceLocation;
using SparkleIT.IPVC.YourAccess.Infrastructure.IoC;


namespace SparkleIT.IPVC.YourAccess.Infrastructure.Web.Container
{
    public class ContainerBootstrapper
    {
        public IWindsorContainer Boot()
        {
            IWindsorContainer container = CreateContainer();         
            SetupServiceLocator(container);
            SetupMvcControllerFactory(container);
            SetupWebApiDependencyResolver(container);
            RegisterLogFacility(container);            
            RunInstallers(container);

            return container;
        }

        public void Cleanup(IWindsorContainer container)
        {
            container.Dispose();
        }

        private IWindsorContainer CreateContainer()
        {
            var container = new WindsorContainer();
            container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel, true));
            return container;
        }

        private void SetupServiceLocator(IWindsorContainer container)
        {
            ServiceLocator.SetLocatorProvider(() => new WindsorServiceLocator(container));
        }

        private void SetupMvcControllerFactory(IWindsorContainer container)
        {
            // MVC
            ControllerBuilder
                .Current
                .SetControllerFactory(new WindsorMvcControllerFactory(container.Kernel));
        }

        private void SetupWebApiDependencyResolver(IWindsorContainer container)
        {            
            GlobalConfiguration.Configuration.DependencyResolver = new WindsorWebApiResolver(container);
            
        }

        private void RegisterLogFacility(IWindsorContainer container)
        {
            container.AddFacility<LoggingFacility>(
                f => f.LogUsing(LoggerImplementation.NLog)
                    .ConfiguredExternally()
                );
        }

        private void RunInstallers(IWindsorContainer container)
        {
            container.Install(FromAssembly.InDirectory(new AssemblyFilter("bin")));
        }

    }
}