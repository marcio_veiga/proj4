﻿using System.Web.Http;
using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace SparkleIT.IPVC.YourAccess.Infrastructure.Web.Container
{
    public class MvcWebApiControllersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            RegisterMvcControllersIn(container);
            RegisterWebAPIControllersIn(container);
        }

        private void RegisterMvcControllersIn(IWindsorContainer container)
        {
            container.Register(Classes.FromAssemblyInDirectory(new AssemblyFilter("bin"))
                .BasedOn<IController>()
                .LifestyleTransient()
                //.Configure(c => c.LifeStyle.HybridPerWebRequestPerThread())
                );
        }

        private void RegisterWebAPIControllersIn(IWindsorContainer container)
        {
            container.Register(Classes.FromAssemblyInDirectory(new AssemblyFilter("bin"))
                .BasedOn<ApiController>()
                .LifestyleTransient()
                //.Configure(c => c.LifeStyle.HybridPerWebRequestPerThread())
                );
        }
    }
}