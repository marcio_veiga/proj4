using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Castle.MicroKernel;

namespace SparkleIT.IPVC.YourAccess.Infrastructure.Web.Container
{
    // APS.NET MVC
    public class WindsorMvcControllerFactory : DefaultControllerFactory
    {
        private readonly IKernel _kernel;

        public WindsorMvcControllerFactory(IKernel kernel)
        {
            _kernel = kernel;
        }

        public override void ReleaseController(IController controller)
        {
            _kernel.ReleaseComponent(controller);
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
            {
                throw new HttpException(404,
                    string.Format("The controller for path '{0}' could not be found.",
                        requestContext.HttpContext.Request.Path));
            }

            return _kernel.Resolve(controllerType) as IController;
        }
    }
}