using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using Castle.Windsor;

namespace SparkleIT.IPVC.YourAccess.Infrastructure.Web.Container
{
    // ASP.NET Webapi
    // http://blog.ploeh.dk/2012/10/03/DependencyInjectioninASP.NETWebAPIwithCastleWindsor/

    public class WindsorWebApiCompositionRoot : IHttpControllerActivator
    {
        private readonly IWindsorContainer _container;

        public WindsorWebApiCompositionRoot(IWindsorContainer container)
        {
            _container = container;
        }

        public IHttpController Create(
            HttpRequestMessage request,
            HttpControllerDescriptor controllerDescriptor,
            Type controllerType)
        {
            var controller = (IHttpController) _container.Resolve(controllerType);

            request.RegisterForDispose(new ReleaseController(() => _container.Release(controller)));

            return controller;
        }

        private class ReleaseController : IDisposable
        {
            private readonly Action _release;

            public ReleaseController(Action release)
            {
                _release = release;
            }

            public void Dispose()
            {
                _release();
            }
        }
    }
}