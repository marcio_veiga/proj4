﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using Castle.Core.Logging;
using ExpressionHelper = Microsoft.Web.Mvc.Internal.ExpressionHelper;

namespace SparkleIT.IPVC.YourAccess.Infrastructure.Web.Mvc
{
    public class FlashMessages
    {
        public const string Error = "flash-message-error";
        public const string Success = "flash-message-success";
        public const string Warning = "flash-message-warning";
    }

    public abstract class AbstractController : Controller
    {
        #region logging

        private ILogger _logger = NullLogger.Instance;

        public ILogger Logger
        {
            get { return _logger; }
            set { _logger = value; }
        }

        #endregion

        public RedirectToRouteResult RedirectToAction<TController>(Expression<Action<TController>> action)
            where TController : Controller
        {
            RouteValueDictionary routingValues = ExpressionHelper.GetRouteValuesFromExpression(action);

            return RedirectToRoute(routingValues);
        }

        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding,
            JsonRequestBehavior behavior)
        {
            return new JSONResult
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior
            };
        }

        #region Messages

        public string[] ModelStateMessages(ModelStateDictionary modelState)
        {
            return (
                from ms in modelState.Values
                from error in ms.Errors
                where !String.IsNullOrWhiteSpace(error.ErrorMessage)
                select error.ErrorMessage
                ).ToArray();
        }

        public void ShowSuccessMessage(string message)
        {
            TempData[FlashMessages.Success] = message;
        }

        public void CleanSuccessMessage()
        {
            if (TempData.ContainsKey(FlashMessages.Success))
            {
                TempData[FlashMessages.Success] = null;
            }
        }

        public void ShowWarningMessage(string message)
        {
            TempData[FlashMessages.Warning] = message;
        }

        public void CleanWarningMessage()
        {
            if (TempData.ContainsKey(FlashMessages.Warning))
            {
                TempData[FlashMessages.Warning] = null;
            }
        }

        public void ShowErrorMessage(string message)
        {
            TempData[FlashMessages.Error] = message;
        }

        public void ShowErrorMessage(IList<string> messageList)
        {
            var message = new StringBuilder();
            foreach (string error in messageList)
            {
                message.AppendFormat("{0}<br/>", error);
            }
            ShowErrorMessage(String.Format("<ul>{0}</ul>", message));
        }

        public void CleanErrorMessage()
        {
            if (TempData.ContainsKey(FlashMessages.Error))
            {
                TempData[FlashMessages.Error] = null;
            }
        }

        public void CleanAllMessage()
        {
            CleanSuccessMessage();
            CleanWarningMessage();
            CleanErrorMessage();
        }

        #endregion

        #region Parameters

        public void SaveParameterForRedirection<T>(string parameterName, T parameterValue)
        {
            if (TempData.ContainsKey(parameterName))
            {
                TempData[parameterName] = parameterValue;
            }
            else
            {
                TempData.Add(parameterName, parameterValue);
            }
        }

        public T RetreiveParameterFromRedirection<T>(string parameterName)
        {
            //Check.Require(TempData.ContainsKey(parameterName), "Cannot retreive parameter with name '" + parameterName + "'!");

            return TempData.ContainsKey(parameterName) ? (T) TempData[parameterName] : default(T);
        }

        public void ClearRedirectionParameter(string parameterName)
        {
            if (TempData.ContainsKey(parameterName))
            {
                TempData.Remove(parameterName);
            }
        }

        #endregion
    }
}