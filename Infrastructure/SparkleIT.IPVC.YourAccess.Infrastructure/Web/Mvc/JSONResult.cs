﻿using System;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SparkleIT.IPVC.YourAccess.Infrastructure.Web.Mvc
{
    // porque estou a fazer isto?
    // 1. o serializador .NET usa datas não compativeis com javascript
    // 2. este serializador é mais rápido que o do .net
    // 3. para configurar a dimensão do output. Por defeito o .net limita a 4Mbytes e em MVC não é configurável!

    public class JSONResult : JsonResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            if (JsonRequestBehavior == JsonRequestBehavior.DenyGet &&
                String.Equals(context.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException("Get verb not allowed");
            }

            HttpResponseBase response = context.HttpContext.Response;

            if (!String.IsNullOrEmpty(ContentType))
            {
                response.ContentType = ContentType;
            }
            else
            {
                response.ContentType = "application/json";
            }
            if (ContentEncoding != null)
            {
                response.ContentEncoding = ContentEncoding;
            }

            if (Data != null)
            {
                JsonSerializer
                    .Create(new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver(),
                        DateFormatHandling = DateFormatHandling.IsoDateFormat
                    })
                    .Serialize(response.Output, Data);
            }
        }
    }
}