﻿using System.Web.Mvc;
using System.Web.Routing;

namespace SparkleIT.IPVC.YourAccess.Infrastructure.Web
{
    public class MvcApplication : AbstractApplication
    {
        protected override void OnApplicationStart()
        {
            SetupViewEngine();
        }

        protected virtual void SetupViewEngine()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());
        }

        protected virtual void SetupDefaultIgnoreRoutes(RouteCollection routes)
        {
            routes.RouteExistingFiles = false;

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.ashx/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new {favicon = @"(.*/)?favicon.([iI][cC][oO]|[gG][iI][fF])(/.*)?"});
            routes.IgnoreRoute("{Content}/{*pathInfo}", new {Content = @"^(.*/)?([Cc])ontent(/.*)?"});
            routes.IgnoreRoute("{Scripts}/{*pathInfo}", new {Scripts = @"^(.*/)?([Ss])cripts(/.*)?"});
            routes.IgnoreRoute("{app}/{*pathInfo}", new {Scripts = @"^(.*/)?([Aa])pp(/.*)?"});
            routes.IgnoreRoute("attachment/{*pathInfo}");
        }
    }
}