﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace SparkleIT.IPVC.YourAccess.Infrastructure.Web.Routing
{
    public static class RouteCollectionExtension
    {
        public static Route MapLowerCaseRoute(this RouteCollection routes, string name, string url, object defaults)
        {
            return routes.MapLowerCaseRoute(name, url, defaults, null);
        }

        public static Route MapLowerCaseRoute(this RouteCollection routes, string name, string url, object defaults,
            object constraints)
        {
            if (routes == null)
            {
                throw new ArgumentNullException("routes");
            }
            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            Route route = new LowercaseRoute(url, new MvcRouteHandler())
            {
                Defaults = new RouteValueDictionary(defaults),
                Constraints = new RouteValueDictionary(constraints),
                DataTokens = new RouteValueDictionary(),
            };

            routes.Add(name, route);

            return route;
        }
    }
}