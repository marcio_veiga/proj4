﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Results;
using System.Web.Http.Routing;
using Castle.Core.Internal;
using Castle.Core.Logging;
using Microsoft.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SparkleIT.IPVC.YourAccess.Infrastructure.Web.WebApi
{   
    public class AbstractApiController : ApiController
    {
        #region logging

        private ILogger _logger = NullLogger.Instance;

        public ILogger Logger
        {
            get { return _logger; }
            set { _logger = value; }
        }

        #endregion

        protected override JsonResult<T> Json<T>(T content, JsonSerializerSettings serializerSettings, Encoding encoding)
        {
            serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            serializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;

            return base.Json(content, serializerSettings, encoding);
        }

        protected StatusCodeResult NoContent()
        {
            return StatusCode(HttpStatusCode.NoContent);
        }

        protected CreatedNegotiatedContentResult<T> ContentCreatedAt<TController, T>(
            Expression<Action<TController>> action, T content)
            where TController : ApiController
        {
            return Created(ApiRoute(action), content);
        }

        protected FileResult File(HttpContent content, string fileName, string mediaType)
        {
            return new FileResult(this, content, fileName, mediaType);
        }

        private static HttpRouteValueDictionary GetParameterValuesFromExpression<TController>(
            Expression<Action<TController>> action) where TController : ApiController
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }
            var call = action.Body as MethodCallExpression;
            if (call == null)
            {
                throw new ArgumentException("You must indicate action");
            }

            ParameterInfo[] parameters = call.Method.GetParameters();
            if (parameters.Length <= 0)
            {
                return null;
            }
            var rvd = new HttpRouteValueDictionary();
            for (int index = 0; index < parameters.Length; ++index)
            {
                Expression expression = call.Arguments[index];
                var constantExpression = expression as ConstantExpression;
                object obj = constantExpression == null
                    ? CachedExpressionCompiler.Evaluate(expression)
                    : constantExpression.Value;
                rvd.Add(parameters[index].Name, obj);
            }

            // por fim "httproute" caso contrário webapi não funciona
            rvd.Add("httproute", "httproute");

            return rvd;
        }


        protected string ApiRoute<TController>(Expression<Action<TController>> action) where TController : ApiController
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }
            var call = action.Body as MethodCallExpression;
            if (call == null)
            {
                throw new ArgumentException("You must indicate action");
            }

            var routePrefixAttribute = typeof (TController).GetAttribute<RoutePrefixAttribute>();
            string routePrefix = routePrefixAttribute != null ? routePrefixAttribute.Prefix : String.Empty;

            var routeAttribute = call.Method.GetAttribute<RouteAttribute>();
            if (routeAttribute == null)
            {
                throw new ArgumentException("You must anotate action with a route attribute");
            }

            var routeFactory = new DirectRouteFactoryContext(
                routePrefix,
                new ReadOnlyCollection<HttpActionDescriptor>(new List<HttpActionDescriptor>()),
                new DefaultInlineConstraintResolver(),
                true);

            IDirectRouteBuilder builder = routeFactory.CreateBuilder(routeAttribute.Template);

            var route = new HttpRoute(builder.Template);

            IHttpVirtualPathData virtualPathData = route.GetVirtualPath(Request,
                GetParameterValuesFromExpression(action));

            return virtualPathData.VirtualPath;
        }
    }
}