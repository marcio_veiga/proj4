using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace SparkleIT.IPVC.YourAccess.Infrastructure.Web.WebApi
{
    public class FileResult : IHttpActionResult
    {
        public FileResult(ApiController controller, HttpContent content, string fileName, string mediaType)
        {
            Content = content;
            FileName = fileName;
            MediaType = mediaType;
            RequestMessage = controller.Request;
        }

        public HttpContent Content { get; private set; }
        public string FileName { get; private set; }
        public string MediaType { get; private set; }
        public HttpRequestMessage RequestMessage { get; private set; }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(Execute());
        }

        private HttpResponseMessage Execute()
        {
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
                httpResponseMessage.RequestMessage = RequestMessage;
                httpResponseMessage.Content = Content;
                httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = FileName
                };
                httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(MediaType);
            }
            catch
            {
                httpResponseMessage.Dispose();
                throw;
            }
            return httpResponseMessage;
        }
    }
}